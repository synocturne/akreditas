-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 20, 2018 at 11:36 AM
-- Server version: 5.5.60
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_retnoer`
--

-- --------------------------------------------------------

--
-- Table structure for table `borang_dokumen`
--

CREATE TABLE `borang_dokumen` (
  `id_dokumen` int(11) NOT NULL,
  `id_keb_dok` int(11) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL COMMENT 'Uploader',
  `status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `borang_dokumen`
--

INSERT INTO `borang_dokumen` (`id_dokumen`, `id_keb_dok`, `nama`, `file`, `id_user`, `status`) VALUES
(1, 1, 'Contoh Dokumen', 'Contoh Dokumen.docx', 3, '0'),
(2, 1, 'INSTRUMEN-MONEV-revisi 2', 'INSTRUMEN-MONEV-revisi 2.pdf', 3, '0'),
(3, 4, 'contoh1', 'contoh1.docx', 4, '1');

-- --------------------------------------------------------

--
-- Table structure for table `borang_elemen`
--

CREATE TABLE `borang_elemen` (
  `id_elemen` int(11) NOT NULL,
  `id_master` int(11) NOT NULL,
  `butir` varchar(10) DEFAULT NULL,
  `elemen` text,
  `bobot` decimal(4,2) DEFAULT NULL,
  `skor` decimal(4,2) DEFAULT NULL,
  `justifikasi` enum('Sangat Kurang','Kurang','Cukup','Baik','Sangat Baik') DEFAULT NULL,
  `penilaian` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `borang_elemen`
--

INSERT INTO `borang_elemen` (`id_elemen`, `id_master`, `butir`, `elemen`, `bobot`, `skor`, `justifikasi`, `penilaian`) VALUES
(1, 1, '1', 'Kejelasan dan kerealistikan visi, misi, tujuan, dan sasaran program studi.', '1.04', '2.50', 'Cukup', 'Memiliki visi, misi, tujuan, dan sasaran yang cukup jelas namun kurang realistik.'),
(2, 1, '2', 'Strategi pencapaian sasaran dengan rentang waktu yang jelas dan didukung oleh dokumen.', '1.04', '2.80', 'Cukup', 'Strategi pencapaian sasaran: (1) dengan tahapan waktu yang jelas, dan cukup realistik (2) didukung dokumen yang cukup lengkap.'),
(3, 1, '3', 'Sosialisasi visi-misi.  Sosialisasi yang efektif tercermin dari tingkat pemahaman seluruh pemangku kepentingan internal yaitu sivitas akademika (dosen dan mahasiswa) dan tenaga kependidikan.', '1.04', '2.50', 'Cukup', 'Kurang dipahami oleh  sivitas akademika  dan tenaga kependidikan.\r\n'),
(4, 2, '1', 'Tata pamong menjamin terwujudnya visi, terlaksananya misi, tercapainya tujuan, berhasilnya strategi yang digunakan secara kredibel, transparan, akuntabel, bertanggung jawab, dan adil.', '1.39', '2.80', 'Cukup', 'Program studi memiliki  tatapamong yang memungkinkan terlaksananya secara cukup konsisten prinsip tatapamong, dan menjamin penyelenggaraan program studi yang memenuhi  3 dari 5 aspek berikut :  (1) Kredible, (2) Transparan, (3) akuntable (4) bertanggung jawab (5) adil'),
(10, 2, '2', 'Karakteristik kepemimpinan yang efektif (kepemimpinan operasional, kepemimpinan organisasi, kepemimpinan publik).', '0.69', '2.50', 'Cukup', 'Kepemimpinan program studi memiliki karakter kepemimpinan yang kuat dalam salah satu dari karakteristik berikut:  (1) kepemimpinan operasional, (2) kepemimpinan organisasi (3) Kepemimpinan publik.'),
(11, 2, '3', 'Sistem pengelolaan fungsional dan operasional program studi mencakup: planning, organizing, staffing, leading, controlling yang efektif dilaksanakan.', '1.39', '2.80', 'Cukup', 'Sistem pengelolaan fungsional dan operasional program studi dilakukan hanya sebagian sesuai dengan SOP dan dokumen kurang lengkap.\r\n'),
(12, 2, '4', 'Pelaksanaan penjaminan mutu di program studi.', '1.39', '2.50', 'Cukup', 'Sistem penjaminan mutu berfungsi sebagian namun  tidak ada umpan balik dan dokumen kurang lengkap.\r\n'),
(13, 2, '5', 'Penjaringan umpan balik  dan tindak lanjutnya.', '0.69', '2.50', 'Cukup', 'Umpan balik hanya diperoleh dari sebagian dan ada tindak lanjut secara insidental.																							\r\n'),
(14, 2, '6', 'Upaya untuk menjamin keberlanjutan (sustainability) program studi.', '0.69', '2.80', 'Cukup', 'Ada bukti sebagian usaha ( > 3) dilakukan .									'),
(15, 8, '1', 'Pedoman tertulis tentang sistem seleksi, perekrutan, penempatan, pengembangan, retensi, dan pemberhentian dosen dan tenaga kependidikan.', '0.72', '2.80', 'Cukup', 'Ada pedoman tertulis yang lengkap; tetapi tidak dilaksanakan.'),
(16, 4, '1', 'Pedoman tertulis tentang sistem seleksi, perekrutan, penempatan, pengembangan, retensi, dan pemberhentian dosen dan tenaga kependidikan.', '0.72', '2.80', 'Cukup', 'Ada pedoman tertulis yang lengkap; tetapi tidak dilaksanakan.'),
(17, 5, '6', 'Upaya perbaikan sistem pembelajaran yang telah dilakukan selama tiga tahun terakhir.', '0.57', '2.80', 'Cukup', 'Upaya perbaikan dilakukan untuk 2 dari 4 yang seharusnya diperbaiki/ ditingkatkan.'),
(18, 6, '1', 'Keterlibatan program studi dalam perencanaan target kinerja, perencanaan kegiatan/ kerja dan perencanaan/alokasi dan pengelolaan dana.', '0.67', '2.50', 'Cukup', 'Program studi dilibatkan dalam perencanaan alokasi, namun pengelolaan dana dilakukan oleh Fakultas/Sekolah Tinggi.'),
(19, 19, '1', 'Kejelasan dan kelengkapan dokumen kebijakan tentang penyusunan dan evaluasi Visi, misi, tujuan, sasaran, rencana strategis dan rencana operasional.', '0.61', '0.00', 'Baik', 'Ada dokumen yang mencakup kebijakan penyusunan dan evaluasi Visi, misi, tujuan, sasaran, rencana strategis dan rencana operasional secara berkala kurang atau setiap 5 tahun.'),
(20, 19, '2', 'Ketersediaan dokumen visi, misi, tujuan, dan sasaran Program Studi.', '0.61', '0.00', 'Baik', '\"(i) Tersedia visi, misi, tujuan, & sasaran yang telah disahkan\r\n(ii)  Tersedia peta linieritas visi, misi, tujuan dan sasaran Prodi dengan FTTI UNJANI\r\n(iii)  Misi Mencakup Tri Dharma PT\r\n(iv) Ada ciri khas Prodi \"'),
(21, 19, '3', 'Kejelasan dan kerealistikan visi, misi, tujuan, dan sasaran Program Studi', '0.61', '0.00', 'Baik', '\"(i) Ada penjelasan dari kata kunci Visi\r\n(ii) Pernyataan Misi mendukung pencapaian Visi\r\n(iii) Terdapat linearitas dalam penyusunan tujuan dan sasaran\r\n(iv) Terdapat satuan waktu dalam pencapaian sasaran Prodi\r\n\"'),
(22, 3, '1.1.a', 'Rasio calon mahasiswa yang ikut seleksi terhadap daya tampung.', '1.95', '3.30', 'Baik', 'Jika 1 < rasio < 5, maka skor  = (3 + Rasio)/2'),
(23, 3, '1.1.b', 'Rasio mahasiswa baru reguler yang melakukan registrasi terhadap calon mahasiswa baru reguler yang lulus seleksi.', '0.65', '4.00', 'Sangat Baik', 'Jika rasio ? 95%, maka skor = 4'),
(24, 3, '1.1.c', 'Rasio mahasiswa baru transfer terhadap mahasiswa baru regular.', '0.65', '4.00', 'Sangat Baik', 'Jika RM ? 0.25, maka skor = 4.'),
(25, 3, '1.1.d', 'Rata-rata Indeks Prestasi Kumulatif (IPK) selama lima tahun terakhir.', '1.30', '4.00', 'Sangat Baik', 'Jika IPK ? 3, maka skor = 4.																												\r\n'),
(26, 7, '1.1', 'Jumlah penelitian yang dilakukan oleh dosen tetap yang bidang keahliannya sesuai dengan PS per tahun, selama 3 tahun.', '3.75', '4.00', 'Sangat Baik', 'Jika NK ? 2, maka skor = 4.'),
(28, 7, '1.2', 'Keterlibatan mahasiswa yang melakukan tugas akhir dalam penelitian dosen.', '1.88', '0.00', 'Sangat Kurang', 'Jika PD = 0%, maka skor = 0.	'),
(29, 7, '1.3', 'Jumlah artikel ilmiah yang dihasilkan oleh dosen tetap yang bidang keahliannya sesuai dengan PS per tahun, selama tiga tahun.', '3.75', '3.90', 'Baik', 'Jika 0 < NK <  6, maka skor = 1 + (NK / 2).'),
(30, 7, '1.4', 'Karya-karya PS/institusi yang telah memperoleh perlindungan Hak atas Kekayaan Intelektual (HaKI) dalam tiga tahun terakhir.', '1.88', '2.00', 'Cukup', '(Tidak ada skor satu)'),
(31, 20, '1', 'Visi, Misi, Tujuan, dan Sasaran serta Strategi Pencapaian', '0.00', '0.00', 'Sangat Kurang', 'BAIK  Memiliki visi, misi, tujuan, dan sasaran yang:  (1)   Jelas. (2)   Realistik. (3)   Saling terkait satu sama lain. (4)   Melibatkan dosen, mahasiswa, tenaga kependidikan dan alumni								'),
(32, 20, '1.2	', 'Visi', '0.00', '0.00', 'Sangat Kurang', ''),
(33, 20, '1.3', 'Misi', '0.00', '0.00', 'Sangat Kurang', ''),
(34, 20, '1.4', 'Tujuan', '0.00', '0.00', 'Sangat Kurang', ''),
(35, 20, '1.5', 'Sasaran dan Strategi Pencapaiannya', '0.00', '0.00', 'Sangat Kurang', ''),
(36, 20, '2', 'Sosialisasi', '0.00', '0.00', 'Sangat Kurang', 'Uraikan upaya penyebaran/sosialisasi visi, misi, dan tujuan program studi serta pemahaman sivitas akademika(dosen dan mahasiswa) dan tenaga kependidikan'),
(37, 28, '1', 'Sistem Tata Pamong', '0.00', '0.00', 'Sangat Kurang', 'Program studi memiliki  tatapamong yang memungkinkan terlaksananya secara cukup konsisten prinsip tatapamong, dan menjamin penyelenggaraan program studi yang memenuhi  3 dari 5 aspek berikut :  (1) Kredible, (2) Transparan, (3) akuntable (4) bertanggung jawab (5) adil																												\r\n'),
(38, 28, '2', 'Karakteristik kepemimpinan yang efektif (kepemimpinan operasional, kepemimpinan organisasi, kepemimpinan publik', '0.00', '0.00', 'Sangat Kurang', 'Kepemimpinan program studi memiliki karakter kepemimpinan yang kuat dalam salah satu dari karakteristik berikut:  (1) kepemimpinan operasional, (2) kepemimpinan organisasi (3) Kepemimpinan publik																												\r\n'),
(39, 21, '1', 'Visi, Misi, Tujuan, dan Sasaran serta Strategi Pencapaian', '0.00', '0.00', 'Baik', '(4) Memiliki visi, misi, tujuan, dan sasaran yang sangat jelas dan sangat realistik (3) Memiliki visi, misi, tujuan, dan sasaran jelas dan realistik (2) Memiliki visi, misi, tujuan, dan sasaran yang cukup jelas namun kurang realistik (1) Memiliki visi, misi, tujuan, dan sasaran yang kurang jelas dan tidak realistik (0) Tidak ada skor nol'),
(40, 28, '3', 'Sistem pengelolaan fungsional dan operasional program studi mencakup: planning, organizing, staffing, leading, controlling yang efektif dilaksanakan.', '0.00', '0.00', 'Cukup', 'Sistem pengelolaan fungsional dan operasional program studi dilakukan hanya sebagian sesuai dengan SOP dan dokumen kurang lengkap.																												\r\n'),
(41, 28, '4', 'Pelaksanaan penjaminan mutu di program studi.	', '0.00', '0.00', 'Sangat Kurang', 'Sistem penjaminan mutu berfungsi sebagian namun  tidak ada umpan balik dan dokumen kurang lengkap.																												\r\n'),
(42, 28, '5', 'Penjaringan umpan balik  dan tindak lanjutnya.', '0.00', '0.00', 'Sangat Kurang', 'Umpan balik hanya diperoleh dari sebagian dan ada tindak lanjut secara insidental.																												\r\n'),
(43, 28, '6', 'Upaya untuk menjamin keberlanjutan (sustainability) program studi.', '0.00', '0.00', 'Sangat Kurang', 'Ada bukti sebagian usaha ( > 3) dilakukan .																												\r\n'),
(44, 21, '1.2', 'Visi', '0.00', '0.00', 'Baik', ''),
(45, 21, '1.3', 'Misi', '0.00', '0.00', 'Baik', ''),
(46, 29, '1.1.a', 'Rasio calon mahasiswa yang ikut seleksi terhadap daya tampung. 	', '0.00', '0.00', 'Sangat Kurang', 'Jika 1 < rasio < 5, maka skor  = (3 + Rasio)/2																												\r\n'),
(47, 21, '1.4', 'Tujuan dan Sasaran', '0.00', '0.00', 'Baik', ''),
(48, 29, '1.1.b	', 'Rasio mahasiswa baru reguler yang melakukan registrasi terhadap calon mahasiswa baru reguler yang lulus seleksi.', '0.00', '0.00', 'Sangat Kurang', 'Jika rasio ? 95%, maka skor = 4.																												\r\n'),
(49, 21, '1.5', 'Strategis Pencapaian', '0.00', '0.00', 'Baik', ''),
(50, 29, '1.1.c	', 'Rasio mahasiswa baru transfer terhadap mahasiswa baru regular.', '0.00', '0.00', 'Sangat Kurang', 'Jika RM ? 0.25, maka skor = 4.																												\r\n																								\r\n'),
(51, 29, '1.1.d	', 'Rata-rata Indeks Prestasi Kumulatif (IPK) selama lima tahun terakhir.', '0.00', '0.00', 'Sangat Kurang', 'Jika IPK ? 3, maka skor = 4.																												\r\n'),
(52, 29, '1.2', 'Penerimaan mahasiswa non-reguler (selayaknya tidak membuat beban dosen sangat berat, jauh melebihi beban ideal sekitar 12 sks).', '0.00', '0.00', 'Sangat Kurang', 'Jumlah mahasiswa yang diterima masih memungkinkan dosen mengajar seluruh mahasiswa dengan total beban lebih dari 13  s.d. 15 sks.																												\r\n'),
(53, 29, '1.3', 'Penghargaan atas prestasi mahasiswa di bidang nalar, bakat dan minat.	', '0.00', '0.00', 'Sangat Kurang', 'Ada bukti penghargaan juara lomba ilmiah, olah raga, maupun seni tingkat wilayah.																												\r\n'),
(54, 29, '1.4.a', 'Persentase kelulusan tepat waktu.	', '0.00', '0.00', 'Sangat Kurang', 'Jika KTW ? 50%, maka skor = 4.																												\r\n'),
(55, 21, '2', 'Sosialisasi', '0.00', '0.00', 'Baik', 'Uraikan upaya penyebaran/sosialisai visi,misi, dan tujuan program studi serta pemahaman sivitas akademika (dosen dan mahasiswa) dan tenaga pendidikan.'),
(56, 22, '1', 'Sistem Tata Pamong', '0.00', '0.00', 'Baik', 'Program studi memiliki  tatapamong yang memungkinkan terlaksananya secara cukup konsisten prinsip tatapamong, dan menjamin penyelenggaraan program studi yang memenuhi  3 dari 5 aspek berikut :  (1) Kredible, (2) Transparan, (3) akuntable (4) bertanggung jawab (5) adil\r\n'),
(57, 22, '2', 'Kepemimpinan', '0.00', '0.00', 'Baik', 'Kepemimpinan program studi memiliki karakter kepemimpinan yang kuat dalam salah satu dari karakteristik berikut:  (1) kepemimpinan operasional, (2) kepemimpinan organisasi (3) Kepemimpinan publik\r\n'),
(58, 22, '3', 'Sistem Pengelolaan', '0.00', '0.00', 'Sangat Kurang', 'Sistem pengelolaan fungsional dan operasional program studi dilakukan hanya sebagian sesuai dengan SOP dan dokumen kurang lengkap.\r\n'),
(59, 22, '4', 'Penjaminan Mutu', '0.00', '0.00', 'Baik', 'Sistem penjaminan mutu berfungsi sebagian namun  tidak ada umpan balik dan dokumen kurang lengkap.\r\n'),
(60, 22, '5', 'Umpan Balik', '0.00', '0.00', 'Baik', 'Umpan balik hanya diperoleh dari sebagian dan ada tindak lanjut secara insidental.\r\n'),
(61, 22, '6', 'Keberlanjutan', '0.00', '0.00', 'Baik', 'Ada bukti sebagian usaha ( > 3) dilakukan .\r\n'),
(62, 23, '1.1.a', 'Rasio calon mahasiswa yang ikut seleksi terhadap daya tampung. ', '0.00', '0.00', 'Baik', 'Jika 1 < rasio < 5, maka skor  = (3 + Rasio)/2\r\n'),
(63, 23, '1.1.b', 'Rasio mahasiswa baru reguler yang melakukan registrasi terhadap calon mahasiswa baru reguler yang lulus seleksi.', '0.00', '0.00', 'Baik', 'Jika rasio ? 95%, maka skor = 4.\r\n'),
(64, 23, '1.1.c', 'Rasio mahasiswa baru transfer terhadap mahasiswa baru regular.', '0.00', '0.00', 'Baik', 'Jika RM ? 0.25, maka skor = 4.\r\n'),
(65, 23, '1.1.d', 'Rata-rata Indeks Prestasi Kumulatif (IPK) selama lima tahun terakhir.', '0.00', '0.00', 'Baik', 'Jika IPK ? 3, maka skor = 4.\r\n'),
(66, 23, '1.2', 'Penerimaan mahasiswa non-reguler (selayaknya tidak membuat beban dosen sangat berat, jauh melebihi beban ideal sekitar 12 sks).', '0.00', '0.00', 'Baik', 'Jumlah mahasiswa yang diterima masih memungkinkan dosen mengajar seluruh mahasiswa dengan total beban lebih dari 13  s.d. 15 sks.\r\n'),
(67, 23, '1.3', 'Penghargaan atas prestasi mahasiswa di bidang nalar, bakat dan minat.', '0.00', '0.00', 'Baik', 'Ada bukti penghargaan juara lomba ilmiah, olah raga, maupun seni tingkat wilayah.\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `borang_keb_dok`
--

CREATE TABLE `borang_keb_dok` (
  `id_keb_dok` int(11) NOT NULL,
  `id_elemen` int(11) NOT NULL,
  `kebutuhan` varchar(255) NOT NULL,
  `pic` varchar(50) DEFAULT NULL,
  `deskripsi` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `borang_keb_dok`
--

INSERT INTO `borang_keb_dok` (`id_keb_dok`, `id_elemen`, `kebutuhan`, `pic`, `deskripsi`) VALUES
(1, 4, 'Dokumen etika dosen', 'Waket 1', ''),
(2, 4, 'Dokumen etika mahasiswa', 'Waket 3', ''),
(3, 4, 'Dokumen etika tenaga kependidikan', 'Waket 2', ''),
(4, 4, 'Dokumen tentang reward dan punishment', 'Waket 2', ''),
(5, 4, 'SOP layanan administrasi', 'Waket 1', ''),
(6, 4, 'SOP layanan perpustakaan', 'Waket 1', ''),
(7, 4, 'SOP layanan lab dan studi', 'Waket 1', ''),
(8, 4, 'SOP layanan lain jika ada', 'Waket 1', ''),
(9, 1, 'pedoman penulisan VMTS', 'Wadek 1', 'pedoman.'),
(10, 1, 'dokumen visi misi ', 'kaprodi', ''),
(11, 23, 'Daftar mahasiswa tahun 2009 s.d. 2016', 'Waket 1', ''),
(12, 25, 'Data lulusan tahun 2009 s.d. 2016 (+IPK)', 'Waket 1', ''),
(13, 25, 'Data mahasiswa dan lulusan dlm grafik 2009 s.d. 2016', 'Waket 1', '');

-- --------------------------------------------------------

--
-- Table structure for table `borang_master`
--

CREATE TABLE `borang_master` (
  `id_master` int(10) NOT NULL,
  `prodi` enum('in','mi','ti','si') NOT NULL DEFAULT 'in',
  `id_tahun` int(11) NOT NULL,
  `tipe_buku` enum('III A','III B') NOT NULL DEFAULT 'III A',
  `standar` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `borang_master`
--

INSERT INTO `borang_master` (`id_master`, `prodi`, `id_tahun`, `tipe_buku`, `standar`, `judul`) VALUES
(1, 'in', 2, 'III A', 1, 'VISI, MISI, TUJUAN DAN SASARAN, SERTA STRATEGI PENCAPAIAN'),
(2, 'in', 2, 'III A', 2, 'TATA PAMONG, KEPEMIMPINAN, SISTEM PENGELOLAAN, DAN PENJAMINAN MUTU'),
(3, 'in', 2, 'III A', 3, 'MAHASISWA DAN LULUSAN'),
(4, 'in', 2, 'III A', 4, 'SUMBER DAYA MANUSIA '),
(5, 'in', 2, 'III A', 5, 'KURIKULUM, PEMBELAJARAN, DAN SUASANA AKADEMIK'),
(6, 'in', 2, 'III A', 6, 'PEMBIAYAAN, SARANA DAN PRASARANA, SERTA SISTEM INFORMASI'),
(7, 'in', 2, 'III A', 7, 'PENELITIAN, PELAYANAN/ PENGABDIAN KEPADA MASYARAKAT, DAN KERJASAMA'),
(12, 'in', 2, 'III B', 1, 'VISI, MISI, TUJUAN DAN SASARAN, SERTA STRATEGI PENCAPAIAN'),
(13, 'in', 2, 'III B', 2, 'TATA PAMONG, KEPEMIMPINAN, SISTEM PENGELOLAAN, DAN PENJAMINAN MUTU'),
(14, 'in', 2, 'III B', 3, 'MAHASISWA DAN LULUSAN'),
(15, 'in', 2, 'III B', 4, 'SUMBER DAYA MANUSIA '),
(16, 'in', 2, 'III B', 5, 'KURIKULUM, PEMBELAJARAN, DAN SUASANA AKADEMIK'),
(17, 'in', 2, 'III B', 6, 'PEMBIAYAAN, SARANA DAN PRASARANA, SERTA SISTEM INFORMASI'),
(18, 'in', 2, 'III B', 7, 'PENELITIAN, PELAYANAN/ PENGABDIAN KEPADA MASYARAKAT, DAN KERJASAMA'),
(19, 'in', 2, 'III A', 8, 'AUDIT MUTU INTERNAL'),
(20, 'in', 7, 'III A', 1, 'VISI, MISI, TUJUAN, DAN SASARAN, SERTA STRATEGI PENCAPAIAN'),
(21, 'mi', 7, 'III A', 1, 'VISI, MISI, TUJUAN DAN SASARAN, SERTA STRATEGIS PENCAPAIAN'),
(22, 'mi', 7, 'III A', 2, 'TATA PAMONG, KEPEMIMPINAN, SISTEM PENGELOLAAN, DAN PENJAMIN MUTU'),
(23, 'mi', 7, 'III A', 3, 'MAHASISWA DAN LULUSAN'),
(24, 'mi', 7, 'III A', 4, 'SUMBER DAYA MANUSIA'),
(25, 'mi', 7, 'III A', 5, 'KURIKULUM, PEMBELAJARAN, DAN SUASANA AKADEMIK'),
(26, 'mi', 7, 'III A', 6, 'PEMBIAYAAN, SARANA DAN PRASRANA, SERTA SISTEM INFORMASI'),
(27, 'mi', 7, 'III A', 7, 'PENELITIAN, PELAYANAN/PENGABDIAN KEPADA MASYARAKAT, DAN KERJASAMA'),
(28, 'in', 7, 'III A', 2, 'STANDAR 2. TATA PAMONG, KEPEMIMPINAN, SISTEM PENGELOLAAN, DAN PENJAMINAN MUTU'),
(29, 'in', 7, 'III A', 3, 'STANDAR 3.KEMAHASISWAAN DAN LULUSAN');

-- --------------------------------------------------------

--
-- Table structure for table `borang_tahun`
--

CREATE TABLE `borang_tahun` (
  `id_tahun` int(11) NOT NULL,
  `tahun` varchar(4) NOT NULL,
  `oleh` int(11) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `borang_tahun`
--

INSERT INTO `borang_tahun` (`id_tahun`, `tahun`, `oleh`, `status`) VALUES
(1, '2014', 1, '1'),
(2, '2017', 3, '1'),
(3, '2018', 3, '1'),
(7, '2019', 3, '1');

-- --------------------------------------------------------

--
-- Table structure for table `ref_jabatan`
--

CREATE TABLE `ref_jabatan` (
  `jabatan_id` int(11) NOT NULL,
  `jabatan_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_jabatan`
--

INSERT INTO `ref_jabatan` (`jabatan_id`, `jabatan_name`) VALUES
(1, 'SPM'),
(2, 'Kaprodi'),
(3, 'Waket 1'),
(4, 'Waket 2'),
(5, 'Waket 3'),
(6, 'LPPM'),
(7, 'Perpus');

-- --------------------------------------------------------

--
-- Table structure for table `ref_user_grup`
--

CREATE TABLE `ref_user_grup` (
  `user_grup_id` int(11) NOT NULL,
  `user_grup_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_user_grup`
--

INSERT INTO `ref_user_grup` (`user_grup_id`, `user_grup_name`) VALUES
(1, 'SPM'),
(2, 'Kaprodi'),
(3, 'Panitia');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_username` varchar(50) NOT NULL,
  `user_password` varchar(32) NOT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `user_role` enum('0','1','2','3','4') NOT NULL DEFAULT '4' COMMENT '0=superuser, 1=spm, 2=kaprodi, 3=panitia, 4=other',
  `user_by` int(11) NOT NULL DEFAULT '1',
  `user_npk` varchar(50) DEFAULT NULL,
  `user_grup_id` int(11) DEFAULT NULL,
  `user_grup` varchar(100) DEFAULT NULL,
  `user_jabatan_id` int(11) DEFAULT NULL,
  `user_jabatan` varchar(100) DEFAULT NULL,
  `user_telp` varchar(20) DEFAULT NULL,
  `user_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_active` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_username`, `user_password`, `user_name`, `user_role`, `user_by`, `user_npk`, `user_grup_id`, `user_grup`, `user_jabatan_id`, `user_jabatan`, `user_telp`, `user_created`, `user_active`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Administrator', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-19 02:37:08', '1'),
(2, 'kaprodi', '3c13922905d2bc454cc35e665335e2fd', 'Kaprodi 1', '2', 1, '2016.13.0087', 2, 'kaprodi', 2, 'kaprodi', '', '2018-07-08 11:50:39', '1'),
(3, 'spm', '51762626b4f785729159fd35eea74deb', 'SPM 1', '1', 1, '2016.13.0086', 3, NULL, 3, NULL, '089786754312', '2018-07-08 04:02:09', '1'),
(8, 'panitia', 'd32b1673837a6a45f795c13ea67ec79e', 'Panitia 1', '3', 2, '0880', 3, NULL, 3, NULL, '', '2018-07-11 13:43:01', '1'),
(9, 'asd', '7815696ecbf1c96e6894b779456d330e', 'asd', '3', 2, '123', 1, NULL, 1, NULL, 'as', '2018-07-11 14:34:13', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `borang_dokumen`
--
ALTER TABLE `borang_dokumen`
  ADD PRIMARY KEY (`id_dokumen`);

--
-- Indexes for table `borang_elemen`
--
ALTER TABLE `borang_elemen`
  ADD PRIMARY KEY (`id_elemen`);

--
-- Indexes for table `borang_keb_dok`
--
ALTER TABLE `borang_keb_dok`
  ADD PRIMARY KEY (`id_keb_dok`);

--
-- Indexes for table `borang_master`
--
ALTER TABLE `borang_master`
  ADD PRIMARY KEY (`id_master`);

--
-- Indexes for table `borang_tahun`
--
ALTER TABLE `borang_tahun`
  ADD PRIMARY KEY (`id_tahun`);

--
-- Indexes for table `ref_jabatan`
--
ALTER TABLE `ref_jabatan`
  ADD PRIMARY KEY (`jabatan_id`);

--
-- Indexes for table `ref_user_grup`
--
ALTER TABLE `ref_user_grup`
  ADD PRIMARY KEY (`user_grup_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `UNIQUE` (`user_username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `borang_dokumen`
--
ALTER TABLE `borang_dokumen`
  MODIFY `id_dokumen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `borang_elemen`
--
ALTER TABLE `borang_elemen`
  MODIFY `id_elemen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `borang_keb_dok`
--
ALTER TABLE `borang_keb_dok`
  MODIFY `id_keb_dok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `borang_master`
--
ALTER TABLE `borang_master`
  MODIFY `id_master` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `borang_tahun`
--
ALTER TABLE `borang_tahun`
  MODIFY `id_tahun` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ref_jabatan`
--
ALTER TABLE `ref_jabatan`
  MODIFY `jabatan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ref_user_grup`
--
ALTER TABLE `ref_user_grup`
  MODIFY `user_grup_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
