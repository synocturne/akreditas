/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 5.6.26 : Database - akreditas
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`akreditas` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `akreditas`;

/*Table structure for table `borang` */

DROP TABLE IF EXISTS `borang`;

CREATE TABLE `borang` (
  `borang_id` int(11) NOT NULL AUTO_INCREMENT,
  `borang_file` varchar(255) NOT NULL,
  `borang_note` text,
  `borang_by` int(11) NOT NULL,
  `borang_uploaded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`borang_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `borang` */

insert  into `borang`(`borang_id`,`borang_file`,`borang_note`,`borang_by`,`borang_uploaded`) values 
(2,'INSTRUMEN-MONEV-revisi 2.pdf','',2,'2018-04-17 11:14:17'),
(3,'INSTRUMEN-MONEV-revisi 2.docx','',2,'2018-04-17 11:27:10'),
(4,'penugasan analisis jurnal .docx','',2,'2018-04-17 11:28:31');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_username` varchar(50) NOT NULL,
  `user_password` varchar(32) NOT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `user_role` enum('0','1','2','3') NOT NULL DEFAULT '3' COMMENT '0=superuser, 1=kaprodi, 2=spm, 3=other',
  `user_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `UNIQUE` (`user_username`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`user_id`,`user_username`,`user_password`,`user_name`,`user_role`,`user_created`) values 
(1,'admin','21232f297a57a5a743894a0e4a801fc3','Administrator','0','2018-04-11 14:13:59'),
(2,'kaprodi','3c13922905d2bc454cc35e665335e2fd','Kaprodi 1','1','2018-04-14 18:13:41'),
(3,'spm','51762626b4f785729159fd35eea74deb','SPM 1','2','2018-04-14 18:16:58');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
