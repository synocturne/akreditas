/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 5.6.26 : Database - akreditas
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`akreditas` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `akreditas`;

/*Table structure for table `borang_dokumen` */

DROP TABLE IF EXISTS `borang_dokumen`;

CREATE TABLE `borang_dokumen` (
  `id_dokumen` int(11) NOT NULL AUTO_INCREMENT,
  `id_keb_dok` int(11) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL COMMENT 'Uploader',
  `status` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_dokumen`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `borang_dokumen` */

insert  into `borang_dokumen`(`id_dokumen`,`id_keb_dok`,`nama`,`file`,`id_user`,`status`) values 
(1,1,'Contoh Dokumen','Contoh Dokumen.docx',3,'0'),
(2,1,'INSTRUMEN-MONEV-revisi 2','INSTRUMEN-MONEV-revisi 2.pdf',3,'0'),
(3,4,'contoh1','contoh1.docx',4,'1');

/*Table structure for table `borang_elemen` */

DROP TABLE IF EXISTS `borang_elemen`;

CREATE TABLE `borang_elemen` (
  `id_elemen` int(11) NOT NULL AUTO_INCREMENT,
  `id_master` int(11) NOT NULL,
  `butir` varchar(10) DEFAULT NULL,
  `elemen` text,
  `bobot` decimal(4,2) DEFAULT NULL,
  `skor` decimal(4,2) DEFAULT NULL,
  `justifikasi` enum('Sangat Kurang','Kurang','Cukup','Baik','Sangat Baik') DEFAULT NULL,
  `penilaian` text,
  PRIMARY KEY (`id_elemen`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `borang_elemen` */

insert  into `borang_elemen`(`id_elemen`,`id_master`,`butir`,`elemen`,`bobot`,`skor`,`justifikasi`,`penilaian`) values 
(1,1,'1','Kejelasan dan kerealistikan visi, misi, tujuan, dan sasaran program studi.',1.04,2.50,'Cukup','Memiliki visi, misi, tujuan, dan sasaran yang cukup jelas namun kurang realistik.'),
(2,1,'2','Strategi pencapaian sasaran dengan rentang waktu yang jelas dan didukung oleh dokumen.',1.04,2.80,'Cukup','Strategi pencapaian sasaran: (1) dengan tahapan waktu yang jelas, dan cukup realistik (2) didukung dokumen yang cukup lengkap.'),
(3,1,'3','Sosialisasi visi-misi.  Sosialisasi yang efektif tercermin dari tingkat pemahaman seluruh pemangku kepentingan internal yaitu sivitas akademika (dosen dan mahasiswa) dan tenaga kependidikan.',1.04,2.50,'Cukup','Kurang dipahami oleh  sivitas akademika  dan tenaga kependidikan.\r\n'),
(4,2,'1','Tata pamong menjamin terwujudnya visi, terlaksananya misi, tercapainya tujuan, berhasilnya strategi yang digunakan secara kredibel, transparan, akuntabel, bertanggung jawab, dan adil.',1.39,2.80,'Cukup','Program studi memiliki tata pamong yang memungkinkan terlaksananya secara cukup konsisten prinsip tatapamong, dan menjamin penyelenggaraan program studi yang memenuhi  3 dari 5 aspek berikut :  (1) Kredible, (2) Transparan, (3) akuntable (4) bertanggung jawab (5) adil'),
(10,2,'2','Karakteristik kepemimpinan yang efektif (kepemimpinan operasional, kepemimpinan organisasi, kepemimpinan publik).',0.69,2.50,'Cukup','Kepemimpinan program studi memiliki karakter kepemimpinan yang kuat dalam salah satu dari karakteristik berikut:  (1) kepemimpinan operasional, (2) kepemimpinan organisasi (3) Kepemimpinan publik.'),
(11,2,'3','Sistem pengelolaan fungsional dan operasional program studi mencakup: planning, organizing, staffing, leading, controlling yang efektif dilaksanakan.',1.39,2.80,'Cukup','Sistem pengelolaan fungsional dan operasional program studi dilakukan hanya sebagian sesuai dengan SOP dan dokumen kurang lengkap.\r\n'),
(12,2,'4','Pelaksanaan penjaminan mutu di program studi.',1.39,2.50,'Cukup','Sistem penjaminan mutu berfungsi sebagian namun  tidak ada umpan balik dan dokumen kurang lengkap.\r\n'),
(13,2,'5','Penjaringan umpan balik  dan tindak lanjutnya.',0.69,2.50,'Cukup','Umpan balik hanya diperoleh dari sebagian dan ada tindak lanjut secara insidental.																							\r\n'),
(14,2,'6','Upaya untuk menjamin keberlanjutan (sustainability) program studi.',0.69,2.80,'Cukup','Ada bukti sebagian usaha ( > 3) dilakukan .									'),
(15,8,'1','Pedoman tertulis tentang sistem seleksi, perekrutan, penempatan, pengembangan, retensi, dan pemberhentian dosen dan tenaga kependidikan.',0.72,2.80,'Cukup','Ada pedoman tertulis yang lengkap; tetapi tidak dilaksanakan.'),
(16,4,'1','Pedoman tertulis tentang sistem seleksi, perekrutan, penempatan, pengembangan, retensi, dan pemberhentian dosen dan tenaga kependidikan.',0.72,2.80,'Cukup','Ada pedoman tertulis yang lengkap; tetapi tidak dilaksanakan.'),
(17,5,'6','Upaya perbaikan sistem pembelajaran yang telah dilakukan selama tiga tahun terakhir.',0.57,2.80,'Cukup','Upaya perbaikan dilakukan untuk 2 dari 4 yang seharusnya diperbaiki/ ditingkatkan.'),
(18,6,'1','Keterlibatan program studi dalam perencanaan target kinerja, perencanaan kegiatan/ kerja dan perencanaan/alokasi dan pengelolaan dana.',0.67,2.50,'Cukup','Program studi dilibatkan dalam perencanaan alokasi, namun pengelolaan dana dilakukan oleh Fakultas/Sekolah Tinggi.');

/*Table structure for table `borang_keb_dok` */

DROP TABLE IF EXISTS `borang_keb_dok`;

CREATE TABLE `borang_keb_dok` (
  `id_keb_dok` int(11) NOT NULL AUTO_INCREMENT,
  `id_elemen` int(11) NOT NULL,
  `kebutuhan` varchar(255) NOT NULL,
  `pic` varchar(50) DEFAULT NULL,
  `deskripsi` text,
  PRIMARY KEY (`id_keb_dok`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `borang_keb_dok` */

insert  into `borang_keb_dok`(`id_keb_dok`,`id_elemen`,`kebutuhan`,`pic`,`deskripsi`) values 
(1,4,'Dokumen etika dosen','Waket 1',''),
(2,4,'Dokumen etika mahasiswa','Waket 3',''),
(3,4,'Dokumen etika tenaga kependidikan','Waket 2',''),
(4,4,'Dokumen tentang reward dan punishment','Waket 2',''),
(5,4,'SOP layanan administrasi','Waket 1',''),
(6,4,'SOP layanan perpustakaan','Waket 1',''),
(7,4,'SOP layanan lab dan studi','Waket 1',''),
(8,4,'SOP layanan lain jika ada','Waket 1','');

/*Table structure for table `borang_master` */

DROP TABLE IF EXISTS `borang_master`;

CREATE TABLE `borang_master` (
  `id_master` int(10) NOT NULL AUTO_INCREMENT,
  `prodi` enum('in','mi','ti','si') NOT NULL DEFAULT 'ti',
  `id_tahun` int(11) NOT NULL,
  `tipe_buku` enum('III A','III B') NOT NULL DEFAULT 'III A',
  `standar` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  PRIMARY KEY (`id_master`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `borang_master` */

insert  into `borang_master`(`id_master`,`prodi`,`id_tahun`,`tipe_buku`,`standar`,`judul`) values 
(1,'ti',2,'III A',1,'VISI, MISI, TUJUAN DAN SASARAN, SERTA STRATEGI PENCAPAIAN'),
(2,'ti',2,'III A',2,'TATA PAMONG, KEPEMIMPINAN, SISTEM PENGELOLAAN, DAN PENJAMINAN MUTU'),
(3,'ti',2,'III A',3,'MAHASISWA DAN LULUSAN'),
(4,'ti',2,'III A',4,'SUMBER DAYA MANUSIA '),
(5,'ti',2,'III A',5,'KURIKULUM, PEMBELAJARAN, DAN SUASANA AKADEMIK'),
(6,'ti',2,'III A',6,'PEMBIAYAAN, SARANA DAN PRASARANA, SERTA SISTEM INFORMASI'),
(7,'ti',2,'III A',7,'PENELITIAN, PELAYANAN/ PENGABDIAN KEPADA MASYARAKAT, DAN KERJASAMA'),
(12,'ti',2,'III B',1,'VISI, MISI, TUJUAN DAN SASARAN, SERTA STRATEGI PENCAPAIAN'),
(13,'ti',2,'III B',2,'TATA PAMONG, KEPEMIMPINAN, SISTEM PENGELOLAAN, DAN PENJAMINAN MUTU'),
(14,'ti',2,'III B',3,'MAHASISWA DAN LULUSAN'),
(15,'ti',2,'III B',4,'SUMBER DAYA MANUSIA '),
(16,'ti',2,'III B',5,'KURIKULUM, PEMBELAJARAN, DAN SUASANA AKADEMIK'),
(17,'ti',2,'III B',6,'PEMBIAYAAN, SARANA DAN PRASARANA, SERTA SISTEM INFORMASI'),
(18,'ti',2,'III B',7,'PENELITIAN, PELAYANAN/ PENGABDIAN KEPADA MASYARAKAT, DAN KERJASAMA');

/*Table structure for table `borang_tahun` */

DROP TABLE IF EXISTS `borang_tahun`;

CREATE TABLE `borang_tahun` (
  `id_tahun` int(11) NOT NULL AUTO_INCREMENT,
  `tahun` varchar(4) NOT NULL,
  `oleh` int(11) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_tahun`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `borang_tahun` */

insert  into `borang_tahun`(`id_tahun`,`tahun`,`oleh`,`status`) values 
(1,'2014',1,'1'),
(2,'2017',3,'1'),
(3,'2018',3,'1');

/*Table structure for table `ref_jabatan` */

DROP TABLE IF EXISTS `ref_jabatan`;

CREATE TABLE `ref_jabatan` (
  `jabatan_id` int(11) NOT NULL AUTO_INCREMENT,
  `jabatan_name` varchar(50) NOT NULL,
  PRIMARY KEY (`jabatan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `ref_jabatan` */

insert  into `ref_jabatan`(`jabatan_id`,`jabatan_name`) values 
(1,'SPM'),
(2,'Kaprodi'),
(3,'Waket 1'),
(4,'Waket 2'),
(5,'Waket 3'),
(6,'LPPM'),
(7,'Perpus');

/*Table structure for table `ref_user_grup` */

DROP TABLE IF EXISTS `ref_user_grup`;

CREATE TABLE `ref_user_grup` (
  `user_grup_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_grup_name` varchar(100) NOT NULL,
  PRIMARY KEY (`user_grup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `ref_user_grup` */

insert  into `ref_user_grup`(`user_grup_id`,`user_grup_name`) values 
(1,'SPM'),
(2,'Kaprodi'),
(3,'Panitia');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_username` varchar(50) NOT NULL,
  `user_password` varchar(32) NOT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `user_role` enum('0','1','2','3','4') NOT NULL DEFAULT '4' COMMENT '0=superuser, 1=spm, 2=kaprodi, 3=panitia, 4=other',
  `user_by` int(11) NOT NULL DEFAULT '1',
  `user_npk` varchar(50) DEFAULT NULL,
  `user_grup_id` int(11) DEFAULT NULL,
  `user_grup` varchar(100) DEFAULT NULL,
  `user_jabatan_id` int(11) DEFAULT NULL,
  `user_jabatan` varchar(100) DEFAULT NULL,
  `user_telp` varchar(20) DEFAULT NULL,
  `user_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_active` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `UNIQUE` (`user_username`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`user_id`,`user_username`,`user_password`,`user_name`,`user_role`,`user_by`,`user_npk`,`user_grup_id`,`user_grup`,`user_jabatan_id`,`user_jabatan`,`user_telp`,`user_created`,`user_active`) values 
(1,'admin','21232f297a57a5a743894a0e4a801fc3','Administrator','0',1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-04-19 09:37:08','1'),
(2,'kaprodi','3c13922905d2bc454cc35e665335e2fd','Kaprodi 1','2',1,'2016.13.0087',2,'kaprodi',2,'kaprodi','','2018-07-08 18:50:39','1'),
(3,'spm','51762626b4f785729159fd35eea74deb','SPM 1','1',1,'2016.13.0086',3,NULL,3,NULL,'089786754312','2018-07-08 11:02:09','1'),
(8,'panitia','panitia','Waket 1','3',2,'0880',3,NULL,3,NULL,'','2018-07-08 12:31:21','1');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
