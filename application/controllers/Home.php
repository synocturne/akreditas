<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index() {
		$data['htitle'] = "Home";
		$data['content'] = "home/v_index";

		$this->load->view('v_main_top', $data);
	}
}