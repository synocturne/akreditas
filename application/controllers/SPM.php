<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spm extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model(array('m_user', 'm_tahun'));
	}

	public function index() {
		header("Location: ".base_url());				
	}

	public function kaprodi() {
		$data['htitle'] = "Manajemen Kaprodi";
		$data['content'] = "spm/v_kaprodi";
		$data['data_kaprodi'] = $this->m_user->get_user('2');

		$this->load->view('v_main_top', $data);
	}

	public function kaprodi_tambah($error=0) {
		$data['htitle'] = "Tambah Kaprodi";
		$data['content'] = "spm/v_kaprodi_tambah";
		$data['ref_grup'] = $this->m_user->get_grup();
		$data['ref_jabatan'] = $this->m_user->get_jabatan();

		if($error==0) {
			$this->session->unset_userdata('f_npk');
			$this->session->unset_userdata('f_name');
			$this->session->unset_userdata('f_username');
			$this->session->unset_userdata('f_telp');
			$this->session->unset_userdata('f_user_grup_id');
			$this->session->unset_userdata('f_jabatan_id');
			$data['f_npk'] = NULL;
			$data['f_name'] = NULL;
			$data['f_username'] = NULL;
			$data['f_telp'] = NULL;
			$data['f_user_grup_id'] = NULL;
			$data['f_jabatan_id'] = NULL;
		} else {
			$data['f_npk'] = $this->session->userdata('f_npk');
			$data['f_name'] = $this->session->userdata('f_name');
			$data['f_username'] = $this->session->userdata('f_username');
			$data['f_telp'] = $this->session->userdata('f_telp');
			$data['f_user_grup_id'] = $this->session->userdata('f_user_grup_id');
			$data['f_jabatan_id'] = $this->session->userdata('f_jabatan_id');
			
			if($error==1) {
				$data['error'] = "Kedua password harus sama";
			} elseif ($error==2) {
				$data['error'] = "Username sudah dipakai";
			} elseif ($error==3) {
				$data['error'] = "Terjadi kesalahan ketika insert ke database";
			}
		}
		
		$this->load->view('v_main_top', $data);
	}

	public function do_kaprodi_tambah() {
		$pass = $this->input->post("password");
		$repass = $this->input->post("repassword");

		$this->session->set_userdata("f_npk", $this->input->post("npk"));
		$this->session->set_userdata("f_name", $this->input->post("name"));
		$this->session->set_userdata("f_username", $this->input->post("username"));
		$this->session->set_userdata("f_telp", $this->input->post("telp"));
		$this->session->set_userdata("f_user_grup_id", $this->input->post("grup_id"));
		$this->session->set_userdata("f_jabatan_id", $this->input->post("jabatan_id"));

		if($pass!=$repass) {
			header("Location: ".base_url()."spm/kaprodi_tambah/1");
		} else {
			$data = array(
				"user_npk" => $this->input->post("npk"),
				"user_name" => $this->input->post("name"),
				"user_username" => $this->input->post("username"),
				"user_password" => md5($this->input->post("password")),
				"user_telp" => $this->input->post("telp"),
				"user_grup_id" => $this->input->post("grup_id"),
				"user_jabatan_id" => $this->input->post("jabatan_id"),
				"user_role" => '2',
				"user_by" => $this->session->userdata('uid'),
			);

			$insert = $this->m_user->insert_user($data);
			if($insert!==true) {
				header("Location: ".base_url()."spm/kaprodi_tambah/".$insert);				
			} else {
				header("Location: ".base_url()."spm/kaprodi");				
			}
		}
	}

	public function kaprodi_edit($id, $error=0) {
		if($id!=NULL) {
			$data['htitle'] = "Edit Kaprodi";
			$data['content'] = "spm/v_kaprodi_edit";
			$data['user_id'] = $id;
			$data['ref_grup'] = $this->m_user->get_grup();
			$data['ref_jabatan'] = $this->m_user->get_jabatan();

			if($error==0) {
				$u = $this->m_user->get_user_by_id($id, '2');

				if(!$u) {
					header("Location: ".base_url()."spm/kaprodi");
				}

				$this->session->unset_userdata('f_npk');
				$this->session->unset_userdata('f_name');
				$this->session->unset_userdata('f_username');
				$this->session->unset_userdata('f_telp');
				$this->session->unset_userdata('f_user_grup_id');
				$this->session->unset_userdata('f_jabatan_id');
				$data['f_npk'] = $u->user_npk;
				$data['f_name'] = $u->user_name;
				$data['f_username'] = $u->user_username;
				$data['f_telp'] = $u->user_telp;
				$data['f_user_grup_id'] = $u->user_grup_id;
				$data['f_jabatan_id'] = $u->user_jabatan_id;
			} else {
				$data['f_npk'] = $this->session->userdata('f_npk');
				$data['f_name'] = $this->session->userdata('f_name');
				$data['f_username'] = $this->session->userdata('f_username');
				$data['f_telp'] = $this->session->userdata('f_telp');
				$data['f_user_grup_id'] = $this->session->userdata('f_user_grup_id');
				$data['f_jabatan_id'] = $this->session->userdata('f_jabatan_id');

				if($error==1) {
					$data['error'] = "Kedua password harus sama";
				} elseif ($error==2) {
					$data['error'] = "Username sudah dipakai";
				} elseif ($error==3) {
					$data['error'] = "Terjadi kesalahan ketika update ke database";
				}
			}

			$this->load->view('v_main_top', $data);
		} else {
			header("Location: ".base_url()."spm/kaprodi");				
		}
	}

	public function do_kaprodi_edit() {
		$id = $this->input->post("user_id");
		$pass = $this->input->post("password");
		$repass = $this->input->post("repassword");
		$data = array();

		$this->session->set_userdata("f_npk", $this->input->post("npk"));
		$this->session->set_userdata("f_name", $this->input->post("name"));
		$this->session->set_userdata("f_username", $this->input->post("username"));
		$this->session->set_userdata("f_telp", $this->input->post("telp"));
		$this->session->set_userdata("f_user_grup_id", $this->input->post("grup_id"));
		$this->session->set_userdata("f_jabatan_id", $this->input->post("jabatan_id"));

		$data = array(
			"user_npk" => $this->input->post("npk"),
			"user_name" => $this->input->post("name"),
			"user_username" => $this->input->post("username"),
			"user_telp" => $this->input->post("telp"),
			"user_grup_id" => $this->input->post("grup_id"),
			"user_jabatan_id" => $this->input->post("jabatan_id")
		);

		if($pass!="") {
			if($pass!=$repass) {
				header("Location: ".base_url()."spm/kaprodi_edit/$id/1"); die;
			} else {
				$data['user_password'] = md5($pass);
			}
		}

		$update = $this->m_user->update_user($id, $data);
		if($update!==true) {
			header("Location: ".base_url()."spm/kaprodi_edit/$id/$update");				
		} else {
			header("Location: ".base_url()."spm/kaprodi");				
		}
	}

	public function do_kaprodi_hapus($id) {
		$this->m_user->delete_kaprodi($id);
		header("Location: ".base_url()."spm/kaprodi");
	}

	public function tahun($error=0) {
		$data['htitle'] = "Tahun Borang";
		$data['content'] = "spm/v_tahun";

		if ($error==2) {
			$data['error'] = "Tahun sudah dipakai";
		} elseif ($error==3) {
			$data['error'] = "Terjadi kesalahan pada database";
		} elseif ($error==4) {
			$data['error'] = "Tidak dapat menghapus tahun apabila sedang digunakan";
		}

		$data['data_tahun'] = $this->m_tahun->get_tahun();

		$this->load->view('v_main_top', $data);
	}

	public function do_tahun_tambah() {
		$data = array(
			"tahun" => $this->input->post("tahun"),
			"oleh" => $this->session->userdata('uid')
		);

		$insert = $this->m_tahun->insert_tahun($data);
		if($insert!==true) {
			header("Location: ".base_url()."spm/tahun/".$insert);				
		} else {
			header("Location: ".base_url()."spm/tahun");				
		}
	}

	public function do_tahun_edit() {
		$id = $this->input->post("id_tahun");
		$data = array(
			"tahun" => $this->input->post("tahun"),
			"status" => $this->input->post("status"),
			"oleh" => $this->session->userdata('uid')
		);

		$update = $this->m_tahun->update_tahun($id, $data);
		if($update!==true) {
			header("Location: ".base_url()."spm/tahun/".$update);				
		} else {
			header("Location: ".base_url()."spm/tahun");				
		}
	}

	public function do_tahun_hapus($id) {
		$delete = $this->m_tahun->delete_tahun($id);
		if($delete!==true) {
			header("Location: ".base_url()."spm/tahun/".$delete);				
		} else {
			header("Location: ".base_url()."spm/tahun");				
		}
	}


}