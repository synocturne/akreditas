<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model(array('m_admin'));
	}

	public function index() {
		header("Location: ".base_url());				
	}

	public function jabatan() {
		$data['htitle'] = "Jabatan";
		$data['content'] = "admin/v_jabatan";
		$data['data_jabatan'] = $this->m_admin->get_jabatan();

		$this->load->view('v_main_top', $data);
	}

	public function jabatan_tambah($error=0) {
		$data['htitle'] = "Tambah Jabatan";
		$data['content'] = "admin/v_jabatan_tambah";
		
		$this->load->view('v_main_top', $data);
	}

	public function do_jabatan_tambah() {
		$insert = $this->m_admin->insert_jabatan($this->input->post());
		if($insert!==true) {
			header("Location: ".base_url()."admin/jabatan_tambah/".$insert);				
		} else {
			header("Location: ".base_url()."admin/jabatan/");				
		}
	}

	public function jabatan_edit($id=NULL) {
		if($id==NULL)
			{ header("Location: ".base_url()); die; }

		$data['htitle'] = "Edit Jabatan";
		$data['content'] = "admin/v_jabatan_edit";
		$data['jabatan_id'] = $id;

		$u = $this->m_admin->get_jabatan_by_id($id);
		$data['f_jabatan_name'] = $u->jabatan_name;

		$this->load->view('v_main_top', $data);
	}

	public function do_jabatan_edit($id=NULL) {
		if($id==NULL)
			{ header("Location: ".base_url()); die; }

		$update = $this->m_admin->update_jabatan($id, $this->input->post());
		if($update!==true) {
			header("Location: ".base_url()."admin/jabatan_edit/$id");				
		} else {
			header("Location: ".base_url()."admin/jabatan/");				
		}
	}

	public function do_jabatan_hapus($id) {
		$this->m_admin->delete_jabatan($id);
		header("Location: ".base_url()."admin/jabatan");
	}
	
	public function grup() {
		$data['htitle'] = "Grup";
		$data['content'] = "admin/v_grup";
		$data['data_grup'] = $this->m_admin->get_grup();

		$this->load->view('v_main_top', $data);
	}

	public function grup_tambah($error=0) {
		$data['htitle'] = "Tambah Grup";
		$data['content'] = "admin/v_grup_tambah";
		
		$this->load->view('v_main_top', $data);
	}

	public function do_grup_tambah() {
		$insert = $this->m_admin->insert_grup($this->input->post());
		if($insert!==true) {
			header("Location: ".base_url()."admin/grup_tambah/".$insert);				
		} else {
			header("Location: ".base_url()."admin/grup/");				
		}
	}

	public function grup_edit($id=NULL) {
		if($id==NULL)
			{ header("Location: ".base_url()); die; }

		$data['htitle'] = "Edit Grup";
		$data['content'] = "admin/v_grup_edit";
		$data['user_grup_id'] = $id;

		$u = $this->m_admin->get_grup_by_id($id);
		$data['f_user_grup_name'] = $u->user_grup_name;

		$this->load->view('v_main_top', $data);
	}

	public function do_grup_edit($id=NULL) {
		if($id==NULL)
			{ header("Location: ".base_url()); die; }

		$update = $this->m_admin->update_grup($id, $this->input->post());
		if($update!==true) {
			header("Location: ".base_url()."admin/grup_edit/$id");				
		} else {
			header("Location: ".base_url()."admin/grup/");				
		}
	}

	public function do_grup_hapus($id) {
		$this->m_admin->delete_grup($id);
		header("Location: ".base_url()."admin/grup");
	}

}