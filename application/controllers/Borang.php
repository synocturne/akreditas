<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Borang extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model(array('m_borang'));
	}

	public function index() {
		header("Location: ".base_url());				
	}

	public function master() {
		$data['htitle'] = "Isian Borang";
		$data['content'] = "borang/v_master";
		$data['data_bo_master'] = $this->m_borang->get_master();

		$this->load->view('v_main_top', $data);
	}

	public function master_tambah($error=0) {
		$data['htitle'] = "Tambah Data Master";
		$data['content'] = "borang/v_master_tambah";

		if($error==0) {
			$this->session->unset_userdata('f_judul');
			$this->session->unset_userdata('f_standar');
			$data['f_judul'] = NULL;
			$data['f_standar'] = NULL;
		} else {
			$data['f_judul'] = $this->session->userdata('f_judul');
			$data['f_standar'] = $this->session->userdata('f_standar');

			if($error==3) {
				$data['error'] = "Terjadi kesalahan ketika insert ke database";
			}
		}
		
		$this->load->view('v_main_top', $data);
	}

	public function do_master_tambah() {
		$this->session->set_userdata("f_standar", $this->input->post("standar"));
		$this->session->set_userdata("f_judul", $this->input->post("judul"));

		$data = array(
			"prodi" => $this->session->userdata("l_prodi"),
			"id_tahun" => $this->session->userdata("l_tahun"),
			"tipe_buku" => $this->session->userdata("l_buku"),
			"standar" => $this->input->post("standar"),
			"judul" => $this->input->post("judul")
		);

		$insert = $this->m_borang->insert_master($data);
		if($insert!==true) {
			header("Location: ".base_url()."borang/master_tambah/".$insert);				
		} else {
			header("Location: ".base_url()."borang/master");
		}
	}

	public function master_edit($id_master=NULL, $error=0) {
		if($id_master==NULL): header('Location: '.base_url());die; endif;

		$data['htitle'] = "Edit Data Master";
		$data['content'] = "borang/v_master_edit";
		$data['id_master'] = $id_master;
		$master = $this->m_borang->get_master_info($id_master);

		if($error==0) {
			$this->session->unset_userdata('f_judul');
			$this->session->unset_userdata('f_standar');
			$data['f_judul'] = $master->judul;
			$data['f_standar'] = $master->standar;
		} else {
			$data['f_judul'] = $this->session->userdata('f_judul');
			$data['f_standar'] = $this->session->userdata('f_standar');

			if($error==3) {
				$data['error'] = "Terjadi kesalahan ketika edit ke database";
			}
		}
		
		$this->load->view('v_main_top', $data);
	}

	public function do_master_edit($id_master=NULL) {
		if($id_master==NULL): header('Location: '.base_url());die; endif;

		$update = $this->m_borang->update_master($id_master, $this->input->post());
		if($update!==true) {
			header("Location: ".base_url()."borang/master_edit/".$update);				
		} else {
			header("Location: ".base_url()."borang/master");
		}
	}

	public function do_delete_master($id_master=NULL) {
		if($id_master==NULL): header('Location: '.base_url());die; endif;

		$this->m_borang->delete_master($id_master);
		header("Location: ".base_url()."borang/master");
	}

	public function elemen($id_master=NULL) {
		if($id_master==NULL): header('Location: '.base_url());die; endif;

		$master = $this->m_borang->get_master_info($id_master);
		$data['htitle'] = "Elemen Penilaian";
		$data['use_tbox'] = true;
		$data['tbox_1'] = "$master->standar - $master->judul";

		$data['content'] = "borang/v_elemen";
		$data['id_master'] = $id_master;
		$data['standar'] = $master->standar;
		$data['data_bo_elemen'] = $this->m_borang->get_elemen($id_master);

		$this->load->view('v_main_top', $data);
	}

	public function elemen_tambah($id_master=NULL, $error=0) {
		if($id_master==NULL): header('Location: '.base_url());die; endif;

		$master = $this->m_borang->get_master_info($id_master);
		$data['htitle'] = "Tambah Elemen Penilaian";
		$data['use_tbox'] = true;
		$data['tbox_1'] = "$master->standar - $master->judul";

		$data['content'] = "borang/v_elemen_tambah";
		$data['id_master'] = $id_master;
		$data['standar'] = $master->standar;
		
		$this->load->view('v_main_top', $data);
	}

	public function do_elemen_tambah($id_master=NULL) {
		if($id_master==NULL): header('Location: '.base_url());die; endif;

		$data = $this->input->post();
		$data["id_master"] = $id_master;
		$insert = $this->m_borang->insert_elemen($data);
		if($insert!==true) {
			header("Location: ".base_url()."borang/elemen/$id_master/$insert");				
		} else {
			header("Location: ".base_url()."borang/elemen/$id_master");
		}
	}

	public function elemen_edit($id_master=NULL, $id_elemen=NULL, $error=0) {
		if($id_master==NULL || $id_elemen==NULL): header('Location: '.base_url());die; endif;

		$elemen = $this->m_borang->get_elemen_info($id_elemen);
		$master = $this->m_borang->get_master_info($elemen->id_master);

		$data['htitle'] = "Edit Elemen Penilaian";
		$data['use_tbox'] = true;
		$data['tbox_1'] = "$master->standar - $master->judul";

		$data['content'] = "borang/v_elemen_edit";
		$data['id_master'] = $elemen->id_master;
		$data['id_elemen'] = $id_elemen;
		$data['standar'] = $master->standar;
		
		$data['f_butir'] = $elemen->butir;
		$data['f_elemen'] = $elemen->elemen;
		$data['f_bobot'] = $elemen->bobot;
		$data['f_skor'] = $elemen->skor;
		$data['f_justifikasi'] = $elemen->justifikasi;
		$data['f_penilaian'] = $elemen->penilaian;

		$this->load->view('v_main_top', $data);
	}

	public function do_elemen_edit($id_master=NULL, $id_elemen=NULL) {
		if($id_elemen==NULL): header('Location: '.base_url());die; endif;

		$update = $this->m_borang->update_elemen($id_elemen, $this->input->post());
		if($update!==true) {
			header("Location: ".base_url()."borang/elemen_edit/$id_master/$id_elemen/$update");				
		} else {
			header("Location: ".base_url()."borang/elemen/$id_master");
		}
	}

	public function do_delete_elemen($id_master=NULL, $id_elemen=NULL) {
		if($id_elemen==NULL | $id_master==NULL): header('Location: '.base_url());die; endif;

		$this->m_borang->delete_elemen($id_elemen);
		header("Location: ".base_url()."borang/elemen/$id_master");
	}

	public function keb_dok($id_elemen=NULL) {
		if($id_elemen==NULL): header('Location: '.base_url());die; endif;

		$elemen = $this->m_borang->get_elemen_info($id_elemen);
		$master = $this->m_borang->get_master_info($elemen->id_master);

		$data['htitle'] = "Kelola Kebutuhan Dokumen";
		$data['use_tbox'] = true;
		$data['tbox_1'] = "$master->standar - $master->judul";
		$data['tbox_2'] = "$master->standar.$elemen->butir - $elemen->elemen";

		$data['use_tbinfo'] = true;
		$data['tbinfo_1'] = $elemen->penilaian;
		$data['tbinfo_2'] = $elemen->justifikasi;

		$data['content'] = "borang/v_keb_dok";
		$data['id_master'] = $elemen->id_master;
		$data['id_elemen'] = $id_elemen;

		$keb_dok = $this->m_borang->get_keb_dok($id_elemen);
		foreach ($keb_dok as $k => $v) {
			$keb_dok[$k]->dokumen = $this->m_borang->get_dokumen($v->id_keb_dok);
		}

		$data['data_bo_keb_dok'] = $keb_dok;

		$this->load->view('v_main_top', $data);
	}

	public function keb_dok_tambah($id_elemen=NULL, $error=0) {
		if($id_elemen==NULL): header('Location: '.base_url());die; endif;

		$elemen = $this->m_borang->get_elemen_info($id_elemen);
		$master = $this->m_borang->get_master_info($elemen->id_master);

		$data['htitle'] = "Tambah Kebutuhan Dokumen";
		$data['use_tbox'] = true;
		$data['tbox_1'] = "$master->standar - $master->judul";
		$data['tbox_2'] = "$master->standar.$elemen->butir - $elemen->elemen";

		$data['content'] = "borang/v_keb_dok_tambah";
		$data['id_elemen'] = $id_elemen;
		
		$this->load->view('v_main_top', $data);
	}

	public function do_keb_dok_tambah($id_elemen=NULL) {
		if($id_elemen==NULL): header('Location: '.base_url());die; endif;

		$data = $this->input->post();
		$data["id_elemen"] = $id_elemen;

		$insert = $this->m_borang->insert_keb_dok($data);
		if($insert!==true) {
			header("Location: ".base_url()."borang/keb_dok/$id_elemen/$insert");				
		} else {
			header("Location: ".base_url()."borang/keb_dok/$id_elemen");
		}
	}

	public function keb_dok_edit($id_keb_dok=NULL, $error=0) {
		if($id_keb_dok==NULL): header('Location: '.base_url());die; endif;

		$keb_dok = $this->m_borang->get_keb_dok_info($id_keb_dok);
		$elemen = $this->m_borang->get_elemen_info($keb_dok->id_elemen);
		$master = $this->m_borang->get_master_info($elemen->id_master);

		$data['htitle'] = "Edit Kebutuhan Dokumen";
		$data['use_tbox'] = true;
		$data['tbox_1'] = "$master->standar - $master->judul";
		$data['tbox_2'] = "$master->standar.$elemen->butir - $elemen->elemen";

		$data['content'] = "borang/v_keb_dok_edit";
		$data['id_elemen'] = $keb_dok->id_elemen;
		$data['id_keb_dok'] = $id_keb_dok;
		
		$data['f_kebutuhan'] = $keb_dok->kebutuhan;
		$data['f_pic'] = $keb_dok->pic;
		$data['f_deskripsi'] = $keb_dok->deskripsi;

		$this->load->view('v_main_top', $data);
	}

	public function do_keb_dok_edit($id_elemen=NULL, $id_keb_dok=NULL) {
		if($id_keb_dok==NULL): header('Location: '.base_url());die; endif;

		$update = $this->m_borang->update_keb_dok($id_keb_dok, $this->input->post());
		if($update!==true) {
			header("Location: ".base_url()."borang/keb_dok_edit/$id_elemen/$update");				
		} else {
			header("Location: ".base_url()."borang/keb_dok/$id_elemen");
		}
	}

	public function do_delete_keb_dok($id_elemen=NULL, $id_keb_dok=NULL) {
		if($id_elemen==NULL | $id_keb_dok==NULL): header('Location: '.base_url());die; endif;

		$this->m_borang->delete_keb_dok($id_keb_dok);
		header("Location: ".base_url()."borang/keb_dok/$id_elemen");
	}

	public function dokumen($id_keb_dok=NULL, $n=0, $error=NULL) {
		if($id_keb_dok==NULL): header('Location: '.base_url());die; endif;

		$keb_dok = $this->m_borang->get_keb_dok_info($id_keb_dok);
		$elemen = $this->m_borang->get_elemen_info($keb_dok->id_elemen);
		$master = $this->m_borang->get_master_info($elemen->id_master);

		$data['htitle'] = "Upload Kebutuhan Dokumen";
		$data['use_tbox'] = true;
		$data['tbox_1'] = "$master->standar - $master->judul";
		$data['tbox_2'] = "$master->standar.$elemen->butir - $elemen->elemen";
		$data['tbox_3'] = "$master->standar.$elemen->butir ($n) - $keb_dok->kebutuhan";

		$data['content'] = "borang/v_dokumen";
		$data['id_master'] = $elemen->id_master;
		$data['id_elemen'] = $keb_dok->id_elemen;
		$data['id_keb_dok'] = $id_keb_dok;
		$data['main_n'] = $n;
		$data['data_bo_dokumen'] = $this->m_borang->get_dokumen($id_keb_dok);

		if($error==1) {
			$data['error'] = "Dokumen dengan nama file yang sama sudah ada.";
		} elseif($error==2) {
			$data['error'] = "Format file tidak diijinkan.";
		} elseif($error==2) {
			$data['error'] = "Gagal menaruh file di server, mohon coba kembali.";
		}
		// echo "<pre>"; var_dump($master); echo "</pre>"; die;

		$this->load->view('v_main_top', $data);
	}

	public function do_upload_dok($id_keb_dok=NULL, $n=NULL) {
		if($id_keb_dok==NULL): header('Location: '.base_url());die; endif;

		$dir = getcwd()."/docs/";
		$file = $dir.basename($_FILES["theFile"]["name"]);
		$ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
		$ok = 1;

		if (file_exists($file)) {
			header("Location: ".base_url()."borang/dokumen/$id_keb_dok/$n/1");
			$ok = 0;
		}

		$allow = array("doc", "docx", "xls", "xlsx", "pdf");
		if (!in_array($ext, $allow)) {
			header("Location: ".base_url()."borang/dokumen/$id_keb_dok/$n/2");
			$ok = 0;
		}

		if ($ok == 1) {
			if (move_uploaded_file($_FILES["theFile"]["tmp_name"], $file)) {
				$name = basename($_FILES["theFile"]["name"]);
				$data = array(
					"id_keb_dok" 	=> $id_keb_dok,
					"nama" 			=> pathinfo($name, PATHINFO_FILENAME),
					"file" 			=> $name,
					"id_user" 		=> $this->session->userdata('uid')
				);

				$this->m_borang->insert_dokumen($data);
				header("Location: ".base_url()."borang/dokumen/$id_keb_dok/$n");
			} else {
				header("Location: ".base_url()."borang/dokumen/$id_keb_dok/$n/3");
			}
		}
	}

	public function do_status_dok($id_keb_dok=NULL, $n=NULL) {
		if($id_keb_dok==NULL): header('Location: '.base_url());die; endif;

		$id_dok = $this->input->post("id_dok");
		$data = array(
			"status" => $this->input->post("status")
		);

		$this->m_borang->update_dokumen($id_dok, $data);
		header("Location: ".base_url()."borang/dokumen/$id_keb_dok/$n");
	}

	public function do_delete_dok($id_keb_dok=NULL, $id_dokumen=NULL, $n=NULL) {
		if($id_dokumen==NULL | $id_keb_dok==NULL): header('Location: '.base_url());die; endif;

		$this->m_borang->delete_dokumen($id_dokumen);
		header("Location: ".base_url()."borang/dokumen/$id_keb_dok/$n");
	}

}