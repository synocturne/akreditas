<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model(array('m_main'));
	}

	public function index($page=null) {
		$data['btitle'] = "Sistem Informasi Monitoring Borang Akreditasi<br/> Program Studi Informatika UNJANI Yogyakarta";

		if(null==$this->session->userdata('uid') || null==$this->session->userdata('uname')) {
			$data['htitle'] = "Login";
			$data['tahun'] = $this->m_main->get_tahun();

			$this->load->view('login/v_login.php', $data);
		} else {
			$this->session->set_userdata('btitle', $data['btitle']);
			// echo "<pre>"; var_dump($this->session->userdata()); echo "</pre>"; die;

			$data['htitle'] = "Home";
			$data['content'] = "home/v_index";

			$data_home = $this->m_main->get_home();
			$data['data_home'] = $data_home;

			$nilai = 0;
			if(sizeof($data_home)>0) {
				foreach ($data_home as $k => $v) {

					if($this->session->userdata('l_buku')=='III A') {
						if($v->standar!="8") {
							$nilai = $nilai + ($v->nilai*75/100);
						} else {
							$nilai = $nilai + ($v->nilai*10/100);
						}

					} elseif($this->session->userdata('l_buku')=='III B') {
						$nilai = $nilai + ($nilai*15/100);
					}
				}
			}
			$data['nilai_akhir'] = $nilai;

			if($nilai>90) {
				$data['status'] = 'A';
			} elseif($nilai>75) {
				$data['status'] = 'B';
			} elseif($nilai>59) {
				$data['status'] = 'C';
			} else {
				$data['status'] = 'TT';
			}

			$this->load->view('v_main_top', $data);
		}
	}

	public function doLogin() {
		$respon = $this->m_main->do_login($this->input->post());

		if($respon != false) {
			foreach ($respon as $key => $value) {
				$this->session->set_userdata($key, $value);
			}
			header('Location: '.base_url());
		} else {
			header('Location: '.base_url().'?failed');
		}
	}

	public function doLogout() {
		$this->session->sess_destroy();
		header('Location: '.base_url());
	}

	public function profile() {
		$data['htitle'] = "Profile";
		$data['content'] = "main/v_profile";
		// $data['data_profile'] = $this->m_kaprodi->get_panitia();

		$this->load->view('v_main_top', $data);
	}

	public function not_found() {
		$head['page_title'] = "404";

		$this->load->view('comp/page_header.php', $head);
		echo "Page not found";
	}
}
