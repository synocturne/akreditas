
<div class="box box-info">
	<div class="box-header" align="right">
		<button class="btn bg-maroon pull-right" data-toggle="modal" data-target="#modal-tahun">+ Tambah Tahun</button>
	</div>
	<div class="box-body">
		<table class="table dtable table-bordered table-striped table-hove">
			<thead>
				<tr>
					<th>#</th>
					<th>Tahun</th>
					<th>Oleh</th>
					<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php $n=1; foreach($data_tahun as $k => $v) { ?> <tr>
					<td align="center"><?=$n?></td>
					<td><?=$v->tahun?></td>
					<td><?=$v->oleh?></td>
					<td align="center"><?=$v->status==1 ? "Aktif" : "Tidak aktif"?></td>
					<td align="center">
						<button class="btn btn-info btn-xs btn-edit" title="Edit" data-toggle="modal" data-target="#modal-tahun-edit" tahun-id="<?=$v->id_tahun?>" tahun="<?=$v->tahun?>">
							<i class="fa fa-fw fa-pencil"></i>
						</button>
						<button class="btn btn-danger btn-xs btn-hapus" title="Hapus" tahun-id="<?=$v->id_tahun?>"><i class="fa fa-fw fa-trash"></i></button>
					</td>
				</tr>
				<?php $n++; } ?>
			</tbody>
		</table>
	</div>
</div>

<form method="post" action="<?=base_url()?>spm/do_tahun_tambah">
	<div class="modal fade" id="modal-tahun" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="modal-content">
					<div class="modal-header">
						<div class="row">
							<div class="col-md-3">
								<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
							</div>
							<div class="col-md-6" align="center">
								<h4 class="modal-title">Tambah Tahun</h4>
							</div>
							<div class="col-md-3" align="right">
								<input type="submit" class="btn bg-maroon" value="Simpan" />
							</div>
						</div>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="year">Masukkan Tahun</label>
							<input class="form-control no-spinner" type="number" min="2000" max="2099" step="1" value="2018" id="year" name="tahun" required>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<form method="post" action="<?=base_url()?>spm/do_tahun_edit">
	<div class="modal fade" id="modal-tahun-edit" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="modal-content">
					<div class="modal-header">
						<div class="row">
							<div class="col-md-3">
								<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
							</div>
							<div class="col-md-6" align="center">
								<h4 class="modal-title">Edit Tahun</h4>
							</div>
							<div class="col-md-3" align="right">
								<input type="submit" class="btn bg-maroon" value="Simpan" />
							</div>
						</div>
					</div>
					<div class="modal-body">
						<input type="hidden" name="id_tahun" id="id_tahun" value="">
						<div class="row">
							<div class="col-sm-8">								
								<div class="form-group">
									<label for="year">Masukkan Tahun</label>
									<input class="form-control no-spinner" type="number" min="2000" max="2099" step="1" value="" id="year-edit" name="tahun" required>
								</div>
							</div>
							<div class="col-sm-4">								
								<div class="form-group">
									<label for="status">Status</label>
									<select class="form-control" name="status" id="status">
										<option value="1">Aktif</option>
										<option value="0">Tidak Aktif</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<script>
	$(function () {
		$(".btn-edit").click(function() {
			var id_tahun = $(this).attr("tahun-id");
			var tahun = $(this).attr("tahun");

			$("#id_tahun").val(id_tahun);
			$("#year-edit").val(tahun);
		});

		$(".btn-hapus").click(function() {
			var r = confirm("Hapus data ini?");
			var id = $(this).attr("tahun-id");

			if (r == true) {
				window.location.replace('<?=base_url()?>spm/do_tahun_hapus/'+id);
			} 
		});
	})
</script>
