<style>
td {
	vertical-align: middle!important; 
}
.fa-check {
	color: #00d472;
}
.fa-close {
	color: red;
}
label:hover {
	cursor: pointer;
}
</style>

<div class="box box-info">
	<div class="box-body">
		<table class="table dtable table-bordered table-striped table-hove">
			<thead>
				<tr>
					<th>Standar</th>
					<th>Dokumen</th>
					<th>Oleh</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<?php $n=1; foreach($data_dokumen as $k => $v) { ?> <tr>
					<td><?=$v->standar.". ".$v->judul?></td>
					<td>
						<?php foreach($v->dokumen as $y => $z) { ?>
						<a href="<?=base_url()?>docs/<?=$z->file?>"><?=$z->nama?></a> <br>
						<?php } ?>
					</td>
					<td>
						<?php foreach($v->dokumen as $y => $z) { ?>
						<?=$z->oleh?> <br>
						<?php } ?>
					</td>
					<td align="center">
						<?php foreach($v->dokumen as $y => $z) { ?>
						<i class="fa fa-fw <?php echo $z->status==1 ? 'fa-check' : 'fa-close'; ?>"></i> <br>
						<?php } ?>
					</td>
				</tr>
				<?php $n++; } ?>
			</tbody>
		</table>
	</div>
</div>