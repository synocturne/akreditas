<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?=$htitle?></title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="stylesheet" href="<?=BASE_URL()?>assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?=BASE_URL()?>assets/css/font-awesome.min.css">
<link rel="stylesheet" href="<?=BASE_URL()?>assets/css/dataTables.bootstrap.min.css">
<!-- <link rel="stylesheet" href="assets/css/ionicons.min.css"> -->
<link rel="stylesheet" href="<?=BASE_URL()?>assets/css/AdminLTE.min.css">
<link rel="stylesheet" href="<?=BASE_URL()?>assets/css/skin-blue-light.css">
<!-- <link rel="stylesheet" href="assets/css/gfonts.css"> -->
<link rel="stylesheet" href="<?=BASE_URL()?>assets/css/custom.css">