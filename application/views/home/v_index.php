<div class="box box-info">
	<div class="box-body">
		<table class="table table-bordered table-striped table-hove" id="tb-home">
			<thead>
				<tr>
					<th>Standar</th>
					<th>Nilai</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($data_home as $k => $v) { ?>
				<tr>
					<td><?=$v->standar?>. <?=$v->judul?></td>
					<td><?=$v->nilai?></td>
				</tr>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td>Nilai Akhir</td>
					<td><?=$nilai_akhir?></td>
				</tr>
				<tr>
					<td>Status</td>
					<td><?=$status?></td>
				</tr>				
			</tfoot>
		</table>
	</div>
</div>

<script>
	$(function() {
		$("#tb-home").DataTable({
			searching: false,
		});	
	});
</script>