<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view("comp/load_head"); ?>
	<style>
	.box {
		margin: 20px auto 0 auto;
		width: 400px;
	}
	h3 {
		text-align: center;
		margin-bottom: 20px;
		line-height: 1.6;
	}
</style>
</head>
<body class="bg-blue">
	<div class="middle" align="center">
		<img src="assets/img/unjani.png" height="120"> <br>
		<h4 style="font-size: 22px">
			Sistem Informasi Monitoring Borang Akreditasi<br>
			Universitas Jenderal Achmad Yani Yogyakarta
		</h4>
		<div class="box box-solid">
			<div class="box-header with-border">
				<h3 class="box-title">Silahkan Login</h3>
			</div>
			<form role="form" method="post" action="main/doLogin">
				<div class="box-body">
					<div class="form-group">
						<input type="text" class="form-control" name="l_user" placeholder="Username" required="true" autofocus>
					</div>
					<div class="form-group">
						<input type="password" class="form-control" name="l_pass" placeholder="Password" required="true">
					</div>
					<div class="form-group">
						<select name="l_prodi" class="form-control" required>
							<option value="">- Program Studi -</option>
							<option value="in">Informatika</option>
							<option value="mi">Manajemen Informatika</option>
							<option value="ti">Teknologi Informasi</option>
							<option value="si">Sistem Informasi</option>
						</select>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<select name="l_tahun" class="form-control" required>
									<option value="">- Tahun -</option>
									<?php foreach ($tahun as $k => $v) { ?>
										<option value="<?=$v->id_tahun?>"><?=$v->tahun?></option>
									<?php } ?>
								</select>
							</div>
						</div>							
						<div class="col-md-6">
							<div class="form-group">
								<select name="l_buku" class="form-control" required>
									<option value="">- Buku Borang -</option>
									<option value="III A">III A</option>
									<option value="III B">III B</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<?php if(isset($_GET['failed'])) { ?> 
								<div class="alert alert-danger alert-dismissible"> 
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> 
									<i class="icon fa fa-ban"></i> Username atau password salah
								</div> 
							<?php } ?> 
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" align="right">
							<input type="submit" value="Login" class="btn bg-maroon" />
						</div>
					</div>
				</div>
			</form>
			<br>
		</div>
	</div>

	<script src="<?=base_url()?>assets/js/jquery.min.js"></script>
	<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
	<script>
		localStorage.clear();
	</script>
</body>
</html>