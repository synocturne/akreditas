<?php if(null==$this->session->userdata('uid') && null==$this->session->userdata('uname')) { redirect(base_url(), 'refresh'); } ?>

<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view("comp/load_head"); ?>
	<style>
	.container {
		width: initial!important;
	}
</style>
</head>
<body class="hold-transition skin-blue-light layout-top-nav fixed">
	<div class="wrapper">
		<header class="main-header">
			<nav class="navbar navbar-static-top">
				<div class="container" style="color: white; padding: 10px">
					<div style="float: left;">
						<b>
							Sistem Informasi Monitoring Borang Akreditasi <br>
							Program Studi Informatika <br>
							Universitas Jenderal Achmad Yani Yogyakarta
						</b>
					</div>
					<div style="float: right;">
						<img src="<?=base_url()?>assets/img/unjani.png" height="60">
					</div>
				</div>
				<div class="container" style="background-color: #66a7ce">
					<div class="navbar-header">
						<a href="<?=base_url()?>" class="navbar-brand hidden">Borang <b>Akreditasi</b></a>
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
							<i class="fa fa-bars"></i>
						</button>
					</div>

					<div class="collapse navbar-collapse pull-left" id="navbar-collapse">
						<ul class="nav navbar-nav">
							<?php $this->load->view('comp/navigation'); ?>
						</ul>
						<form class="navbar-form navbar-left hidden" role="search">
							<div class="form-group">
								<input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
							</div>
						</form>
					</div>
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<li class="dropdown user user-menu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<img src="<?=base_url()?>assets/img/avatar2.png" class="user-image" alt="User Image">
									<span class="hidden-xs"><?=$this->session->userdata('uname')?></span>
								</a>

								<ul class="dropdown-menu">
									<li class="user-header">
										<img src="<?=base_url()?>assets/img/avatar2.png" class="img-circle" alt="User Image">
										<p><b><?=$this->session->userdata('name')?></b></p>
										<p><?php ?></p>
									</li>
									<li class="user-body">
										<div class="row">
											<div class="col-xs-7 text-center">
												<?=$this->session->userdata('i_prodi')?>
											</div>
											<div class="col-xs-2 text-center">
												<?=$this->session->userdata('i_tahun')?>
											</div>
											<div class="col-xs-3 text-center">
												<?=$this->session->userdata('i_buku')?>
											</div>
										</div>
									</li>
									<li class="user-footer">
										<div class="pull-left">
											<!-- <a href="<?=base_url()?>main/profile" class="btn btn-default">Profile</a> -->
										</div>
										<div class="pull-right">
											<a id="btn-logout" class="btn bg-maroon">Log out</a>
										</div>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</header>

		<script src="<?=base_url()?>assets/js/jquery.min.js"></script>
		<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>

		<div class="content-wrapper" style="padding-top: 135px">
			<div class="container">
				<section class="content-header">
					<h4><b><?=$htitle?></b></h4>

					<?php if(isset($error)) { ?>
						<div class="alert alert-danger alert-dismissible" style="margin-bottom: 0">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<i class="icon fa fa-ban"></i> <?=$error?>
						</div>
						<br>
					<?php } ?>

					<?php if(isset($use_tbox) && $use_tbox==true) { ?>
						<div class="box box-solid">
							<div class="box-body">
								<?php echo isset($tbox_1) ? "<h4>$tbox_1</h4>" : ""; ?>
								<?php echo isset($tbox_2) ? "<h5 style='font-size: 16px'>$tbox_2</h5>" : ""; ?>
								<?php echo isset($tbox_3) ? "<h5>$tbox_3</h5>" : ""; ?>

								<?php if(isset($use_tbinfo) && $use_tbinfo==true) { ?>
									<table class="table table-bordered">
										<tr>
											<td>Standar Penilaian</td>
											<td><?=$tbinfo_1?></td>
										</tr>
										<tr>
											<td>Justifikasi</td>
											<td><?=$tbinfo_2?></td>
										</tr>
									</table>
								<?php } ?>
							</div>					
						</div>
					<?php } ?>
				</section>

				<section class="content container-fluid" id="contentDiv">
					<?php $this->load->view($content); ?>
				</section>
			</div>
		</div>

		<footer class="main-footer">
			<div class="container">
				<div class="pull-right hidden-xs">
					<!-- Anything you want -->
				</div>
				<div align="center">
					<strong>Universitas Jenderal Achmad Yani Yogyakarta</strong>
				</div>
			</div>
		</footer>
	</div>

	<script src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script>
	<script src="<?=base_url()?>assets/js/dataTables.bootstrap.min.js"></script>
	<script src="<?=base_url()?>assets/js/sweetalert2.js"></script>
	<script src="<?=base_url()?>assets/js/fastclick.js"></script>
	<script src="<?=base_url()?>assets/js/adminlte.min.js"></script>
	<script>
		$(function() {
			$("#btn-logout").click(function() {
				swal({
					title: 'Yakin untuk log out?',
					type: 'question',
					showCancelButton: true,
					cancelButtonColor: '#d33',
					confirmButtonColor: '#3085d6',
					cancelButtonText: 'Tidak',
					confirmButtonText: 'Ya!'
				}).then((result) => {
					if (result.value) {
						window.location.replace('<?=base_url()?>main/doLogout');
					}
				})
			});

			$(".dtable").DataTable();

			var actnav = localStorage['act-nav'];
			if(actnav) {
				$("#"+actnav).addClass("active");
			} else {
				$("#nav_home").addClass("active");
			}

			$(".nav-link").click(function() {
				localStorage['act-nav'] = $(this).attr("id");
			});

			$(".btn-hapus").click(function() {
				var url = $(this).attr("del-url");
				
				swal({
					title: 'Hapus data ini?',
					type: 'warning',
					showCancelButton: true,
					cancelButtonColor: '#d33',
					confirmButtonColor: '#3085d6',
					cancelButtonText: 'Tidak',
					confirmButtonText: 'Ya!'
				}).then((result) => {
					if (result.value) {
						window.location.replace(url);
					}
				})			
			});
		});
	</script>
</body>
</html>