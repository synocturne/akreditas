<div class="box box-info">
	<form method="post" action="<?=base_url()?>borang/do_keb_dok_tambah/<?=$id_elemen?>">
		<div class="box-header">
			<a href="<?=base_url()?>borang/keb_dok/<?=$id_elemen?>" class="btn btn-default"><i class="fa fa-fw fa-arrow-left"></i> Batal</a>
			<input type="submit" class="btn bg-maroon pull-right" value="Simpan" />
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-8">
					<div class="form-group">
						<label>Kebutuhan Dokumen</label>
						<input type="text" class="form-control" required="true" name="kebutuhan" />
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>PIC</label>
						<input type="text" class="form-control" required="true" name="pic" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Deskripsi</label>
						<textarea class="form-control" rows="4" name="deskripsi"></textarea>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
