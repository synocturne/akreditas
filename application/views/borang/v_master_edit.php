<div class="box box-info">
	<form method="post" action="<?=base_url()?>borang/do_master_edit/<?=$id_master?>">
		<div class="box-header">
			<a href="<?=base_url()?>borang/master" class="btn btn-default"><i class="fa fa-fw fa-arrow-left"></i> Batal</a>
			<input type="submit" class="btn bg-maroon pull-right" value="Simpan" />
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label>Standar</label>
						<input type="number" class="form-control no-spinner" name="standar" required="true" value="<?=$f_standar?>" />
					</div>
				</div>
				<div class="col-md-10">
					<div class="form-group">
						<label>Judul</label>
						<input type="text" class="form-control" name="judul" required="true" value="<?=$f_judul?>" />
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
