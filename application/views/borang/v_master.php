<div class="box box-info">
	<?php if($this->session->userdata('role')==1) { ?>
		<div class="box-header" align="right">
			<a href="<?=base_url()?>borang/master_tambah" class="btn bg-maroon">+ Tambah Data Master</a>
		</div>
	<?php } ?>
	<div class="box-body">
		<table class="table dtable table-bordered table-striped table-hove">
			<thead>
				<tr>
					<th>#</th>
					<th>Judul Data Master</th>
					<th>Elemen Penilaian</th>
					<th>Kelola</th>
					<?php if($this->session->userdata('role')==1) { echo "<th>Aksi</th>"; } ?>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($data_bo_master as $k => $v) { ?>
					<tr>
						<td>Standar <?=$v->standar?></td>
						<td><?=$v->judul?></td>
						<td align="center"><?=$v->jml_elemen?></td>
						<td align="center">
							<a href="<?=base_url()?>borang/elemen/<?=$v->id_master?>" class="btn bg-orange btn-xs"><i class="fa fa-fw fa-list-ul"></i> Elemen Penilaian</a>
						</td>
						<?php if($this->session->userdata('role')==1) { ?>
							<td align="center">
								<a href="<?=base_url()?>borang/master_edit/<?=$v->id_master?>" class="btn btn-info btn-xs" title="Edit"><i class="fa fa-fw fa-pencil"></i></a>&nbsp;
								<button class="btn btn-danger btn-xs btn-hapus" title="Hapus" del-url='<?=base_url("borang/do_delete_master/$v->id_master")?>'><i class="fa fa-fw fa-trash"></i></button>
							</td>
						<?php } ?>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>