<div class="box box-info">
	<form method="post" action="<?=base_url()?>borang/do_elemen_tambah/<?=$id_master?>">
		<div class="box-header">
			<a href="<?=base_url()?>borang/elemen/<?=$id_master?>" class="btn btn-default"><i class="fa fa-fw fa-arrow-left"></i> Batal</a>
			<input type="submit" class="btn bg-maroon pull-right" value="Simpan" />
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label>Poin</label>
						<div class="input-group">
							<span class="input-group-addon"><?=$standar?>.</span>
							<input type="text" class="form-control" name="butir" required="true" autofocus />
						</div>
					</div>
				</div>
				<div class="col-md-10">
					<div class="form-group">
						<label>Elemen Penilaian</label>
						<input type="text" class="form-control" name="elemen" required="true" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>Bobot</label>
						<input type="text" class="form-control" name="bobot" />
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Nilai</label>
						<input type="text" class="form-control" name="skor" />
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Justifikasi</label>
						<select class="form-control" name="justifikasi">
							<option value="Sangat Kurang">SANGAT KURANG</option>
							<option value="Kurang">KURANG</option>
							<option value="Cukup">CUKUP</option>
							<option value="Baik">BAIK</option>
							<option value="Sangat Baik">SANGAT BAIK</option>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Standard Nilai</label>
						<textarea class="form-control" rows="4" name="penilaian"></textarea>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
