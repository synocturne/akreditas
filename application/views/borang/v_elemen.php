<div class="box box-info">
	<div class="box-header">
		<a href="<?=base_url()?>borang/master" class="btn btn-default"><i class="fa fa-fw fa-arrow-left"></i> Kembali</a>
		<?php if($this->session->userdata('role')==1) { ?>
			<a href="<?=base_url()?>borang/elemen_tambah/<?=$id_master?>" class="btn bg-maroon pull-right">+ Tambah Elemen Penilaian</a>
		<?php } ?>
	</div>
	<div class="box-body">
		<table class="table dtable table-bordered table-striped table-hove">
			<thead>
				<tr>
					<th>Butir</th>
					<th>Elemen Penilaian</th>
					<th>Bobot</th>
					<th>Skor</th>
					<th>Justifikasi</th>
					<th>Dokumen Pendukung</th>
					<?php if($this->session->userdata('role')==1) { echo "<th>Aksi</th>"; } ?>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($data_bo_elemen as $k => $v) { ?>
					<tr>
						<td><?=$standar?>.<?=$v->butir?></td>
						<td><?=$v->elemen?></td>
						<td align="center"><?=$v->bobot?></td>
						<td align="center"><?=$v->skor?></td>
						<td align="center"><?=$v->justifikasi?></td>
						<td align="center">
							<a href="<?=base_url()?>borang/keb_dok/<?=$v->id_elemen?>" class="btn btn-success btn-xs"><i class="fa fa-fw fa-list-ul"></i> Kelola Dokumen</a>
						</td>
						<?php if($this->session->userdata('role')==1) { ?>
							<td align="center">
								<a href="<?=base_url()?>borang/elemen_edit/<?=$id_master?>/<?=$v->id_elemen?>" class="btn btn-info btn-xs" title="Edit"><i class="fa fa-fw fa-pencil"></i></a>&nbsp;
								<button class="btn btn-danger btn-xs btn-hapus" title="Hapus" del-url='<?=base_url("borang/do_delete_elemen/$v->id_master/$v->id_elemen")?>'><i class="fa fa-fw fa-trash"></i></button>
							</td>
						<?php } ?>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>