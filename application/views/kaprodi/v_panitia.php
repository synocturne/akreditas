<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title hidden">Data Table</h3>
		<a href="<?=base_url()?>kaprodi/panitia_tambah" class="btn bg-maroon pull-right">+ Tambah Panitia</a>
	</div>
	<div class="box-body">
		<table class="table dtable table-bordered table-striped table-hove">
			<thead>
				<tr>
					<th>NPK</th>
					<th>Grup</th>
					<th>Username</th>
					<th>Nama</th>
					<th>Jabatan</th>
					<th>Telp</th>
					<th>Tanggal Dibuat</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($data_panitia as $k => $v) { ?>
				<tr>
					<td><?=$v->user_npk?></td>
					<td><?=$v->user_grup_name?></td>
					<td><?=$v->user_username?></td>
					<td><?=$v->user_name?></td>
					<td><?=$v->jabatan_name?></td>
					<td><?=$v->user_telp?></td>
					<td><?=$v->user_created?></td>
					<td align="center">
						<a href="<?=base_url()?>kaprodi/panitia_edit/<?=$v->user_id?>" class="btn btn-info btn-xs" title="Edit"><i class="fa fa-fw fa-pencil"></i></a>&nbsp;
						<button class="btn btn-danger btn-xs btn-hapus" panitia="<?=$v->user_id?>" title="Hapus"><i class="fa fa-fw fa-trash"></i></button>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>

<script>
	$(function () {
		$(".btn-hapus").click(function() {
			var r = confirm("Hapus data ini?");
			var id = $(this).attr("panitia");

			if (r == true) {
				window.location.replace('<?=base_url()?>kaprodi/do_panitia_hapus/'+id);
			} 
		});
	})
</script>
