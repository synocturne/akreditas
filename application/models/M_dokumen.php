<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class M_dokumen extends CI_Model {

	function get_dokumen_old() {
		$this->db->select(
			'bd.*,
			us.user_name AS oleh'
		);
		$this->db->join('user us', 'us.user_id=bd.id_user', 'left');
		$query = $this->db->get('borang_dokumen bd')->result();
		
		return $query;
	}

	function get_dokumen() {
		// echo "<pre>";
		$this->db->select('id_master, standar, judul');
		$this->db->where('prodi', $this->session->userdata('l_prodi'));
		$this->db->where('id_tahun', $this->session->userdata('l_tahun'));
		$this->db->where('tipe_buku', $this->session->userdata('l_buku'));
		$master = $this->db->get('borang_master')->result();

		foreach ($master as $a => $b) {
			$b->dokumen = array();
			$elemen = $this->db->get_where('borang_elemen', array('id_master' => $b->id_master))->result();
			foreach ($elemen as $c => $d) {
				$keb_dok = $this->db->get_where('borang_keb_dok', array('id_elemen' => $d->id_elemen))->result();
				foreach ($keb_dok as $e => $f) {
					$this->db->select(
						'bd.*,
						us.user_name AS oleh'
					);
					$this->db->join('user us', 'us.user_id=bd.id_user', 'left');
					$dokumen = $this->db->get_where('borang_dokumen bd', array('id_keb_dok' => $f->id_keb_dok))->result();
					// $dokumen = $this->db->get_where('borang_dokumen', array('id_keb_dok' => $f->id_keb_dok))->result();
					foreach ($dokumen as $g => $h) {
						array_push($b->dokumen, $h);
					}
				}
			}
		}

		// var_dump($master);
		// die;
		return $master;
	}

}