<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class M_tahun extends CI_Model {

	function get_tahun() {
		$this->db->select('bt.*, u.user_name AS oleh');
		$this->db->from('borang_tahun bt');
		$this->db->join('user u', 'bt.oleh = u.user_id', 'left');
		$this->db->order_by("tahun", "asc");
		$query = $this->db->get()->result();
		return $query;
	}

	function insert_tahun($data) {
		$this->db->where("tahun", $data['tahun']);
		$count = $this->db->get('borang_tahun')->result();
		
		if(sizeof($count)>0) {
			return 2;
		} else {
			$this->db->insert('borang_tahun', $data);

			if($this->db->affected_rows()>0) {
				return true;
			} else {
				return 3;
			}
		}
	}

	function update_tahun($id, $data) {
		$this->db->where("tahun", $data['tahun']);
		$this->db->where("id_tahun !=", $id);
		$count = $this->db->get('borang_tahun')->result();
		
		if(sizeof($count)>0) {
			return 2;
		} else {
			$this->db->where('id_tahun', $id);
			$this->db->update('borang_tahun', $data);

			if($this->db->affected_rows()>0) {
				return true;
			} else {
				return 3;
			}
		}
	}

	function delete_tahun($id) {
		$this->db->where("id_tahun", $id);
		$count = $this->db->get('borang_master')->result();
		
		if(sizeof($count)>0) {
			return 4;
		} else {
			$this->db->where('id_tahun', $id);
			$this->db->delete('borang_tahun'); 

			if($this->db->affected_rows()>0) {
				return true;
			} else {
				return 3;
			}
		}
	}

}