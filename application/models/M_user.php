<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class M_user extends CI_Model {

	function get_user($role) {
		$this->db->select('ua.*, ub.user_name AS user_by_name, g.user_grup_name, j.jabatan_name');
		$this->db->from('user ua');
		$this->db->join('user ub', 'ua.user_by = ub.user_id', 'left');
		$this->db->join('ref_user_grup g', 'ua.user_grup_id = g.user_grup_id', 'left');
		$this->db->join('ref_jabatan j', 'ua.user_jabatan_id = j.jabatan_id', 'left');
		$this->db->where('ua.user_role', $role);
		$query = $this->db->get()->result();
		return $query;
	}

	function get_user_by_id($id, $role) {
		$this->db->where('user_id', $id);
		$this->db->where('user_role', $role);
		$query = $this->db->get('user')->result();

		if(sizeof($query)==1) {
			return $query[0];
		} else {
			return false;
		}
	}

	function insert_user($data) {
		$this->db->where("user_username", $data['user_username']);
		$count = $this->db->get('user')->result();
		
		if(sizeof($count)>0) {
			return 2;
		} else {
			$this->db->insert('user', $data);

			if($this->db->affected_rows()>0) {
				return true;
			} else {
				return 3;
			}
		}
	}

	function update_user($id, $data) {
		$this->db->where("user_username", $data['user_username']);
		$this->db->where("user_id !=", $id);
		$count = $this->db->get('user')->result();
		
		if(sizeof($count)>0) {
			return 2;
		} else {
			$this->db->where('user_id', $id);
			$this->db->update('user', $data);

			if($this->db->affected_rows()>0) {
				return true;
			} else {
				return 3;
			}
		}
	}

	function delete_user($id, $role) {
		$this->db->where('user_id', $id);
		$this->db->delete('user'); 
	}

	function get_grup() {
		$query = $this->db->get('ref_user_grup')->result();
		return $query;
	}

	function get_jabatan() {
		$query = $this->db->get('ref_jabatan')->result();
		return $query;
	}

}