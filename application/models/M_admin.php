<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class M_admin extends CI_Model {

	function get_jabatan() {
		$query = $this->db->get('ref_jabatan')->result();
		return $query;
	}

	function get_jabatan_by_id($id) {
		$this->db->where('jabatan_id', $id);
		$query = $this->db->get('ref_jabatan')->result();
		return $query[0];
	}

	function insert_jabatan($data) {
		$this->db->insert('ref_jabatan', $data);

		if($this->db->affected_rows()>0) {
			return true;
		} else {
			return false;
		}
	}

	function update_jabatan($id, $data) {
		$this->db->where('jabatan_id', $id);
		$this->db->update('ref_jabatan', $data);

		if($this->db->affected_rows()>0) {
			return true;
		} else {
			return false;
		}
	}

	function delete_jabatan($id) {
		$this->db->where('jabatan_id', $id);
		$this->db->delete('ref_jabatan'); 
	}

	function get_grup() {
		$query = $this->db->get('ref_user_grup')->result();
		return $query;
	}

	function get_grup_by_id($id) {
		$this->db->where('user_grup_id', $id);
		$query = $this->db->get('ref_user_grup')->result();
		return $query[0];
	}

	function insert_grup($data) {
		$this->db->insert('ref_user_grup', $data);

		if($this->db->affected_rows()>0) {
			return true;
		} else {
			return false;
		}
	}

	function update_grup($id, $data) {
		$this->db->where('user_grup_id', $id);
		$this->db->update('ref_user_grup', $data);

		if($this->db->affected_rows()>0) {
			return true;
		} else {
			return false;
		}
	}

	function delete_grup($id) {
		$this->db->where('user_grup_id', $id);
		$this->db->delete('ref_user_grup'); 
	}

}